<?php

namespace App\Repositories\Designation;

interface DesignationRepositoryContract
{
    public function getAllDesignation();

    public function getDesignation($id);
    
    public function createDesignation($request);

    public function updateDesignation($request,$id);

    public function deleteDesignation($id);

}