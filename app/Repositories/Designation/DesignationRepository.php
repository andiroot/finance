<?php

namespace App\Repositories\Designation;

use Illuminate\Database\Eloquent\Model;
use App\Models\Payroll\Designation;
use App\Traits\RepositoryTrait;
use DB;

class DesignationRepository 
{
    use RepositoryTrait;
    protected $model;

    public function __construct(Designation $model)
    {
        $this->mode = $model;
    }

    public function getAllDesignation()
    {
        return Designation::latest()->paginate(10);
    }

    public function getDesignation($id)
    {
        return Designation::find($id);
    }
    
    public function createDesignation($request)
    {
        return Designaiton::create([
            'name'      =>$request['name'],
            'enabled'   =>$request['enabled'],

        ]);
    }

    public function updateDesignation($request,$id)
    {
        $data = Designation::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;

    }

    public function deleteDesignation($id)
    {
        $data =  Designation::where('id',$id);
        $data->delete();
        return $data;
    }
}