<?php

namespace App\Repositories\Package;

use Illuminate\Database\Eloquent\Model;
use App\Models\Inventory\Package;
use App\Models\Inventory\Product;
use App\Traits\RepositoryTrait;
use DB;

class PackageRepository 
{

    use RepositoryTrait;
    protected $model;

    public function __construct(Package $model)
    {
        $this->model = $model;
    }
        public function getAllPackage()
        {
            // return Customer::join('provinces','provinces.id','customers.province_id')
            // ->join('cities','cities.id','customers.city_id')
            // ->join('counties','counties.id','customers.county_id')
            // ->select('customers.*','provinces.name as province_id','cities.name as city_id','counties.name as county_id' )->get();
            return Package::latest()->paginate(10);

        }

        public function getPackage($id)
        {
            return Package::find($id);
        }

        public function createPackage($request)
        {
            return Package::create([
                'name'=>$request['name'],
                'sales_price'=>$request['sales_price'],
                'purchase_price'=>$request['purchase_price'],
                'product_id'=>$request['product_id'],
                'qty'=>$request['qty'],     
            ]);

        }

        public function updatePackage($request,$id)
        {
            $data = Package::find($id);
            $data->update($request->all());
            if($data->save()){
                return true;
            }
            return false;
        }

        public function deletePackage($id)
        {
            $data =  Package::where('id',$id);
            $data->delete();
            return $data;
        }

}


