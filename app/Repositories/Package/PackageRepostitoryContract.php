<?php

namespace App\Repositories\Package;

interface PackageRepositoryContract
{
    public function getAllPackage();

    public function getPackage($id);
    
    public function createPackage($request);

    public function updatePackage($request,$id);

    public function deletePackage($id);

}