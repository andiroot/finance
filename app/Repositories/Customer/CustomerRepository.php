<?php

namespace App\Repositories\Customer;

use Illuminate\Database\Eloquent\Model;
use App\Models\Income\Customer;
use App\Models\Common\Province;
use App\Models\Common\City;
use App\Models\Common\County;
use App\Traits\RepositoryTrait;
use DB;

class CustomerRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Customer $model)
    {
        $this->model = $model;
    }

    public function getAllCustomer()
    {
        //  return Customer::join('provinces','provinces.id','customers.province_id')
        //  ->join('cities','cities.id','customers.city_id')
        //  ->join('counties','counties.id','customers.county_id')
        //  ->select('customers.name',
        //           'customers.id',
        //           'customers.email',
        //           'customers.phone',
        //           'customers.address',
        //           'customers.enabled',
        //           'customers.created_at',
        //           'customers.updated_at',
        //           'customers.deleted_at',
        //           'provinces.name as province_id','cities.name as city_id','counties.name as county_id' 
        //           )->latest()->paginate(10);
        return Customer::latest()->paginate(10);
    }

    public function getCustomer($id)
    {

       return Customer::find($id);
        
        // join('provinces','provinces.id','customers.province_id')
        // ->join('cities','cities.id','customers.city_id')
        // ->join('counties','counties.id','customers.county_id')
        // ->select('customers.*','provinces.name as province_id','cities.name as city_id','counties.name as county_id' )
        // ->where('id',$id)
        // ->first();
        

    }

    public function createCustomer($request)
    {
        return Customer::create([
            'name'=>        $request['name'],
            'email'=>       $request['email'],
            'address'=>     $request['address'],
            'phone'=>       $request['phone'],
            'province_id'=> $request['province_id'],
            'city_id'=>     $request['city_id'],
            'county_id'=>   $request['county_id'],
            'enabled'=>     $request['enabled'],
        ]);
        
    }

    public function updateCustomer($request,$id)
    {
        $data = Customer::find($id);
        
        if($data->save()){
            return true;
        }
        return false;

        
    }


    public function deleteCustomer($id)
    {
        $data =  Customer::where('id',$id);
        $data->delete();
        return $data;
        
    }
}