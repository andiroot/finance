<?php

namespace App\Repositories\Customer;

interface CustomerRepositoryContract
{
    public function getAllCustomer();

    public function getCustomer($id);
    
    public function createCustomer($request);

    public function updateCustomer($request,$id);

    public function deleteCustomer($id);

}