<?php

namespace App\Repositories\Customer;

use Illuminate\Database\Eloquent\Model;
use App\Models\Banking\Account;
use App\Models\Banking\Reconciliation;
use App\Traits\RepositoryTrait;
use DB;

class ReconciliationRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Reconciliation $model)
    {
        $this->model = $model;
    }

    public function getAllReconciliation()
    {
        //  return Customer::join('provinces','provinces.id','customers.province_id')
        //  ->join('cities','cities.id','customers.city_id')
        //  ->join('counties','counties.id','customers.county_id')
        //  ->select('customers.name',
        //           'customers.id',
        //           'customers.email',
        //           'customers.phone',
        //           'customers.address',
        //           'customers.enabled',
        //           'customers.created_at',
        //           'customers.updated_at',
        //           'customers.deleted_at',
        //           'provinces.name as province_id','cities.name as city_id','counties.name as county_id' 
        //           )->latest()->paginate(10);
        return Reconciliation::latest()->paginate(10);
    }

    public function getReconciliation($id)
    {

       return Reconciliation::find($id);
        
        // join('provinces','provinces.id','customers.province_id')
        // ->join('cities','cities.id','customers.city_id')
        // ->join('counties','counties.id','customers.county_id')
        // ->select('customers.*','provinces.name as province_id','cities.name as city_id','counties.name as county_id' )
        // ->where('id',$id)
        // ->first();
        

    }

    public function createReconciliation($request)
    {
        return Reconciliation::create([
            'account_id'=>$request['account_id'],
            'started_at'=>$request['started_at'],
            'ended_at'=>$request['ended_at'],
            'closing_balance'=>$request['closing_balance'],
            'reconciled'=>$request['reconciled'],
            
            ]);
        
    }

    public function updateReconciliation($request,$id)
    {
        $data = Reconciliation::find($id);
        
        if($data->save()){
            return true;
        }
        return false;

        
    }


    public function deleteReconciliation($id)
    {
        $data =  Reconciliation::where('id',$id);
        $data->delete();
        return $data;
        
    }
}