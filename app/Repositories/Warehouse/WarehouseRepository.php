<?php
namespace App\Repositories\Warehouse;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Inventory\Warehouse;
use App\Transformers\Warehouse as Transformer;
use DB;

class WarhouseRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Warehouse $model)
    {
        $this->model = $model;
    }

    public function getAllWarhouse()
    {
        return Warehouse::latest()->paginate(10);
        //penggunaaan collection
        //$accounts = Account::all();
        //$accounts = $accounts->where('id',1);
        // $filtered = $accounts->filter(function ($value,$key){
        //     return $value->id <5;
        // });
        // $sorted = $accounts->sortBy('name');//sortByDesc
        // return $sorted->values()->all();
        // $accounts=$accounts->groupBy('createdAt');
        // $length=$accounts->map(function( $value,$key){
        //     return strlen($value->name);
        // });

        // return $length->toArray();


    }
    
    public function getWarehouse($id)
    {   
        $warehouse = Warehouse::find($id);
        return  $warehouse ;
    }

    public function createWarehouse($request)
    {
        return Warehouse::create([
            'name'=>$request['name'],
            'province_id'=>$request['province_id'],
            'city_id'=>$request['city_id'],
            'county_id'=>$request['county_id'],
            'address'=>$request['address'],
            'product_id'=>$request['product_id'],
            'package_id'=>$request['package_id'],
        ]);
        // $accounts= new Account;
        // $accounts->name=$request->name;
        // $accounts->bank_number=$request->bank_number;
        // $accounts->bank_phone=$request->bank_phone;
        // $accounts->opening_balance=$request->opening_balance;
        // $accounts->enabled=$request->enabled;
        // $accounts->bank_address=$request->bank_address;
        // $accounts->type=$request->type;
        // $accounts->save();
        
    }

    public function updateWarehouse($request,$id)
    {
        $warehouse = Warehouse::find($id);
        $warehouse->update($request->all());

        if($account->save()){
            return true;
        }
        return false;
    }

    public function deleteWarehouse($id)
    {
        $warehouse = Warehouse::where('id',$id);
        $warehouse->delete();
        return $warehouse;
    }
}
