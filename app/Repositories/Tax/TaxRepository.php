<?php

namespace App\Repositories\Tax;

use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Tax;
use App\Traits\RepositoryTrait;
use DB;

class TaxRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Tax $model)
    {
        $this->model = $model;
    }

    public function getAllTax()
    {
        return Tax::latest()->paginate(10);
    }

    public function getTax($id)
    {
        return Tax::find($id);

    }

    public function createTax($request)
    {
        return Tax::create([
            'name',
            'rate',
            'enabled',
        ]);
    }

    public function updateTax($request,$id)
    {
        $data = Tax::find($id);
        if($data->save()){
            return true;
        }
        return false;
    }

    public function deleteTax($id)
    {
        $data=Tax::where('id',$id);
        $data->delete();
        return $data;
    }
}

