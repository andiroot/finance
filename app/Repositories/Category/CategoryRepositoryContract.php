<?php

namespace App\Repositories\Category;

interface CategoryRepositoryContract
{
    public function getAllCategory();
    
    public function getCategory($id);

    public function createCategory($request);

    public function updateCategory($request);

    public function deleteCategory($id);
}