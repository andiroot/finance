<?php

namespace App\Repositories\Category;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Inventory\Category;
use DB;

class CategoryRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Category $model)
    {
        $this->mode=$model;
    }

    public function getAllCategory()
    {
        return Category::latest()->paginate(10);
    }

    public function getCategory($id)
    {
      $data = Category::find($id);
      return $data;
    }

    public function createCategory($request)
    {
        return Category::creaete([
            'type'      =>$request['type'],
            'name'      =>$request['name'],
            'color'     =>$request['color'],
            'enabled'   =>$request['enabled'],
        ]);
    }

    public function updateCategory( $request,$id)
    {
        $update = Category::find($id);
        $update->update($request->all());

        if($update->save()){
            return true;
        }
        return false;
    }

    public function deleteCategory($id)
    {
        $delete = Category::where('id',$id);
        $delete->delete();
        return $delete;
    }

}