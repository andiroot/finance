<?php

namespace App\Repositories\Employee;

use Illuminate\Database\Eloquent\Model;
use App\Models\Payroll\Employee;
use App\Traits\RepositoryTrait;
use DB;


class EmployeeRepository 
{
    use RepositoryTrait;
    protected $model;

    public function __construct(Employee $model)
    {
        $this->model=$model;
    }

    public function getAllEmployee()
    {
        return Employee::latest()->paginate(10);
    }

    public function getEmployee($id)
    {
        return Employee::find($id);
    }

    public function createEmployee($request)
    {
        return Employee::create([
            'branch_id'     => $request['branch_id'],
            'user_id'       => $request['user_id'],
            'department_id' => $request['department_id'],
            'designation_id'=> $request['designation_id'],
            'account_id'    => $request['account_id'],
            'nip'           => $request['nip'],
            'address'       => $request['address'],
            'phone'         => $request['phone'],
            'province_id'   => $request['province_id'],
            'city_id'       => $request['city_id'],
            'county_id'     => $request['county_id'],
        ]);

    }

    public function updateEmployee($request,$id)
    {
        $data = Employee::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;

    }

    public function deleteEmployee($id)
    {
        $data =  Employee::where('id',$id);
        $data->delete();
        return $data;
    }
}
