<?php

namespace App\Repositories\Employee;

interface EmployeeRepositoryContract
{
    public function getAllEmployee();

    public function getEmployee($id);
    
    public function createEmployee($request);

    public function updateEmployee($request,$id);

    public function deleteEmployee($id);

}