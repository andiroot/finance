<?php

namespace App\Repositories\Transfer;

interface TransferRepositoryContract
{
    public function getAllTransfer();

    public function getTransfer($id);
    
    public function createTransfer($request);

    public function updateTransfer($request,$id);

    public function deleteTransfer($id);

}