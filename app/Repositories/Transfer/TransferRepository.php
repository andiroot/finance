<?php

namespace App\Repositories\Transfer;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Payment;
use App\Models\Common\Revenue;
use App\Models\Banking\Transfer;
use DB;

class TransferRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Transfer $model)
    {
        $this->model = $model;
    }

    public function getAllTransfer()
    {
        // return Transfer::join('revenues','revenues.id','transfers.revenue_id')
        // ->join('payments','payments.id','transfers.payment_id')
        // ->select('transfers.*','payments.name as payment_id','revenues.name as revenue_id' )->get();
        return Transfer::latest()->paginate(10);
    }

    public function getTransfer($id)
    {
        return $data = DB::select(
            'select * from transfers 
            where id= :id',['id'=>$id]);
    }
    
    public function createTransfer($request)
    {
        return Transfer::create([
            'payment_id'=>$request['payment_id'],
            'revenue_id'=>$request['revenue_id'],
        ]);
    }

    public function updateTransfer($request,$id)
    {
        $data = Transfer::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;
    }

    public function deleteTransfer($id)
    {
        $data =  Transfer::where('id',$id);
        $data->delete();
        return $data;
    }
}
