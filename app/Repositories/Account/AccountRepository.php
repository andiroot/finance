<?php
namespace App\Repositories\Account;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Banking\Account;
use App\Transformers\Account as Transformer;
use DB;

class AccountRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Account $model)
    {
        $this->model = $model;
    }

    public function getAllAccount()
    {
        return Account::latest()->paginate(10);
        //penggunaaan collection
        //$accounts = Account::all();
        //$accounts = $accounts->where('id',1);
        // $filtered = $accounts->filter(function ($value,$key){
        //     return $value->id <5;
        // });
        // $sorted = $accounts->sortBy('name');//sortByDesc
        // return $sorted->values()->all();
        // $accounts=$accounts->groupBy('createdAt');
        // $length=$accounts->map(function( $value,$key){
        //     return strlen($value->name);
        // });

        // return $length->toArray();


    }
    
    public function getAccount($id)
    {   
        $account = Account::find($id);
        return  $account ;
    }

    public function createAccount($request)
    {
        return Account::create([
            'name'              =>$request['name'],
            'bank_number'    =>$request['bank_number'],
            'bank_name'         =>$request['bank_name'],
            'bank_phone'        =>$request['bank_phone'],
            'opening_balance'   =>$request['opening_balance'],
            'enabled'           =>$request['enabled'],
            'bank_address'      =>$request['bank_address'],
            'type'              =>$request['type'],
        ]);
        // $accounts= new Account;
        // $accounts->name=$request->name;
        // $accounts->bank_number=$request->bank_number;
        // $accounts->bank_phone=$request->bank_phone;
        // $accounts->opening_balance=$request->opening_balance;
        // $accounts->enabled=$request->enabled;
        // $accounts->bank_address=$request->bank_address;
        // $accounts->type=$request->type;
        // $accounts->save();
        
    }

    public function updateAccount($request,$id)
    {
        $account = Account::find($id);
        $account->update($request->all());

        if($account->save()){
            return true;
        }
        return false;
    }

    public function deleteAccount($id)
    {
        $account = Account::where('id',$id);
        $account->delete();
        return $account;
    }
}
