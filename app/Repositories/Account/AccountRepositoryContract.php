<?php

namespace App\Repositories\Account;

interface AccountRepositoryContract
{
    public function getAllAccount();

    public function getAccount($id);

    public function createAccount($request);

    public function updateAccount($request,$id);

    public function deleteAccount($id);
}