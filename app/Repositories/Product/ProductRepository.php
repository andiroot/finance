<?php

namespace App\Repositories\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\Inventory\Product;
use App\Traits\RepositoryTrait;
use DB;

class ProductRepository 
{
    use RepositoryTrait;
    protected $model;

    public function __construct(Product $model)
    {
        $this->model=$model;
    }

    public function getAllProduct()
    {
        return Product::latest()->paginate(10);
        // return Product::join('provinces','provinces.id','customers.province_id')
        // ->join('cities','cities.id','customers.city_id')
        // ->join('counties','counties.id','customers.county_id')
        // ->select('customers.*','provinces.name as province_id','cities.name as city_id','counties.name as county_id' )->get();
    }

    public function getProduct($id)
    {
        return Product::find($id);
    }

    public function createProduct($request)
    {
        return Product::create([
            'vendor_id'=>$request['vendor_id'],
            'category_id'=>$request[        'category_id'],
            'uom_id'=>$request[        'uom_id'],
            'tax_id'=>$request[        'tax_id'],
            'name'=>$request[        'name'],
            'sku'=>$request[        'sku'],
            'description'=>$request[        'description'],
            'sale_price'=>$request[        'sale_price'],
            'purchase_price'=>$request[        'purchase_price'],
            'capital_price'=>$request[        'capital_price'],
            'qty'=>$request[        'qty'],
            'weight'=>$request[        'weight'],
            'price_closing'=>$request[        'price_closing'],
            'enabled'=>$request[        'enabled'],
        ]);
    }

    public function updateProduct($request,$id)
    {

        $data = Product::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;
    }

    public function deleteProduct($id)
    {
        $data =  Product::where('id',$id);
        $data->delete();
        return $data;
    }
}