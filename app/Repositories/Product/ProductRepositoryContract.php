<?php

namespace App\Repositories\Product;

interface ProductRepositoryContract
{
    public function getAllProduct();

    public function getProduct($id);
    
    public function createProduct($request);

    public function updateProduct($request,$id);

    public function deleteProduct($id);

}