<?php

namespace App\Repositories\Inventory;

use Illuminate\Database\Eloquent\Model;
use App\Models\Inventory\Inventory;
use App\Traits\RepositoryTrait;
use DB;

class InventoryRepository 
{
        use RepositoryTrait;
        protected $model;

        public function __construct(Inventory $model)
        {
            $this->model = $model;
        }

        public function getAllInventory()
        {
            return Inventory::latest()->paginate(10);
        }

        public function getInventory($id)
        {
            return Inventory::find($id);
        }

        public function createInventory($request)
        {
            return Inventory::create([
                'product_id'        =>$request['product_id'],
                'customer_id'       =>$request['customer_id'],
                'vendor_id'         =>$request['vendor_id'],
                'type'              =>$request['type'],
                'qty'               =>$request['qty'],
                'tran_date'         =>$request['tran_date'],
                'status'            =>$request['status'],
            ]);
        }

        public function updateInventory($request,$id)
        {
            $data = Inventory::find($id);
            $data->update($request->all());
            if($data->save()){
                return true;
            }
            return false;
    
        }

        public function deleteInventory($id)
        {
            $data =  Inventory::where('id',$id);
            $data->delete();
            return $data;
        }

}
