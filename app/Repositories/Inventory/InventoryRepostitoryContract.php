<?php

namespace App\Repositories\Inventory;

interface InventoryRepositoryContract
{
    public function getAllInventory();

    public function getInventory($id);
    
    public function createInventory($request);

    public function updateInventory($request,$id);

    public function deleteInventory($id);

}