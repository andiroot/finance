<?php

namespace App\Repositories\Order;

interface OrderRepositoryContract
{
    public function getAllOrder();

    public function getOrder($id);
    
    public function createOrder($request);

    public function updateOrder($request,$id);

    public function deleteOrder($id);

}