<?php

namespace App\Repositories\Order;

use Illuminate\Database\Eloquent\Model;
use App\Models\Income\Order;
use App\Traits\RepositoryTrait;
use Auth;
use DB;

class OrderRepository 
{
    use RepositoryTrait;
    protected $model;

    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    public function getAllOrder()
    {
        // return Order::join('provinces','provinces.id','customers.province_id')
        // ->join('cities','cities.id','customers.city_id')
        // ->join('counties','counties.id','customers.county_id')
        // ->select('customers.*','provinces.name as province_id','cities.name as city_id','counties.name as county_id' )->get();
        return Order::latest()->paginate(10);
    }

    public function getOrder($id)
    {
        return Order::find($id);
    }
    
    public function createOrder($request)
    {
        return Order::create([
           'order_number'   =>$request['order_number'],
           'shipping_id'    =>$request['shipping_id'],
           'customer_id'    =>$request['customer_id'],
           'employee_id'    =>$request['employee_id'],
           'product_id'     =>$request['product_id'],
           'payment_method' =>$request['payment_method'],
           'status'         =>$request['status'],
           'net_profit'     =>$request['net_profit'],
           'processed_at'   =>$request['processed_at'],
           'processed_by'   =>Auth::id(),
        ]);
    }

    public function updateOrder($request,$id)
    {
        $data = Order::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;
    }

    public function deleteOrder($id)
    {
        $data =  Order::where('id',$id);
        $data->delete();
        return $data;
    }
}