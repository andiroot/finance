<?php

namespace App\Repositories\Department;

interface DepartmentRepositoryContract
{
    public function getAllDepartment();

    public function getDepartment($id);
    
    public function createDepartment($request);

    public function updateDepartment($request,$id);

    public function deleteDepartment($id);

}