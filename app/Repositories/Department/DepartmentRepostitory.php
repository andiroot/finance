<?php

namespace App\Repositories\Department;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Payroll\Department;
use DB;

class DepartmentRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Department $model)
    {
        $this->model = $model;
    }

    public function getAllDepartment()
    {
        return Department::latest()->paginate(10);
    }

    public function getDepartment($id)
    {
        return Department::find($id);
    }

    public function createDepartment($request)
    {
        return Department::create([
            'name'          =>$request['name'],
            'base_salary'   =>$request['base_salary'],
            'base_overtime' =>$request['base_overtime'],
            'allowance'     =>$request['allowance'],
            'enabled'       =>$request['enabled'],
        ]);
    }

    public function updateDepartment($request,$id)
    {
        $data = Department::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;

    }

    public function deleteDepartment($id)
    {
        $data =  Department::where('id',$id);
        $data->delete();
        return $data;
    }

}






