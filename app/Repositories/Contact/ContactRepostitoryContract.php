<?php
namespace App\Repositories\Contact;

interface ContactRepositoryContract
{
    public function getAllContact();

    public function getContact($id);

    public function createContact($requst);

    public function updateContact($requst,$id);

    public function deleteContact($id);
}