<?php

namespace App\Repositories\Contact;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Contact;
use DB;

class ContactRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Contact $model)
    {
        $this->model = $model;
    }

    public function getAllContact()
    {
        return Contact::latest()->paginate(10);
    }

    public function getContact($id)
    {
        $data = Contact::where('id',$id);
        return $data;
    }

    public function createContact($request)
    {
        return Contact::create([
        'name'      =>$request['name'],
        'type'      =>$request['type'],
        'email'     =>$request['email'],
        'phone'     =>$request['phone'],
        'address'   =>$request['address'],
        'enabled'    =>$request['enabled'],
        ]);
    }

    public function updateContact($request,$id)
    {
        $contact = Contact::find($id);
        $contact->update($request->all());

        if($contact->save()){
            return true;
        }
        return false;
    }

    public function deleteContact($id)
    {
        $contact = Contact::where('id',$id);
        $contact->delete();
        return $contact;
    }
}
