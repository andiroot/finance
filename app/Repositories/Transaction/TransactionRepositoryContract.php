<?php

namespace App\Repositories\Transaction;

interface TransactionRepositoryContract
{
    public function getAllTransaction();

    public function getTransaction($id);
    
    public function createTransaction($request);

    public function updateTransaction($request,$id);

    public function deleteTransaction($id);

}