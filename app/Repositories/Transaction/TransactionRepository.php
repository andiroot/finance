<?php

namespace App\Repositories\Transaction;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Banking\Transaction;
use App\Models\Banking\Account;
use App\Models\Common\Contact;
use App\Models\Inventory\Category;
use DB;

class TransactionRepository implements TransactionRepositoryContract
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Transaction $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return Transaction::latest()->paginate(10);
    }

    public function getAllTransaction()
    {
        return Transaction::join('accounts','accounts.id','transactions.account_id')
        ->join('contacts','contacts.id','transactions.contact_id')
        ->join('categories','categories.id','transactions.category_id')
        ->select('transactions.*','account.name as account_id','categories.name as category_id','contacts.name as contact_id' )->get();
    }

    public function getTransaction($id)
    {
        $data = $this->model->getById($id);
        return $data;
    }
    
    public function createTransaction($request)
    {
        return Transaction::create([
            'type'          =>$request['type'],//string
            'account_id'    =>$request['account_id'],
            'paid_at'       =>$request['paid_at'],
            'amount'        =>$request['amount'],
            'currency_code' =>$request['currency_code'],
            'currency_rate' =>$request['currency_rate'],
            'contact_id'    =>$request['contact_id'],
            'description'   =>$request['description'],
            'category_id'   =>$request['category_id'],
            'payment_method'=>$request['payment_method'],
            'references'    =>$request['references'],
            'reconciled'    =>$request['reconciled'] 
        ]);
    }
    
    public function updateTransaction($request,$id)
    {
        $data = Transaction::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;

    }
    
    public function deleteTransaction($id)
    {
        $data =  Transaction::where('id',$id);
        $data->delete();
        return $data;
    }


}