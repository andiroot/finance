<?php

namespace App\Repositories\Branch;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Payroll\Branch;
use DB;

class BranchRepository 
{
    use RepositoryTrait;

    protected $model;
    
    public function __contruct(Branch $model)
    {
        $this->model=$model;
    }

    public function getAllBranch()
    {
        return Branch::latest()->paginate(10);
    }

    public function getBranch()
    {
        return Branch::find($id);
    }

    public function createBranch($request)
    {
        return Branch::create([
            'name'=>$request['name'],
            'address'=>$request['address'],
            'province_id'=>$request['province_id'],
            'city_id'=>$request['city_id'],
            'county_id'=>$request['county_id'],
            'enabled'=>$request['enabled'],
        ]);
    }

    public function updateBranch($request,$id)
    {
        $data = Branch::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;
    }

    public function deleteBranch($id)
    {
        $data = Branch::where('id',$id);
        $data->delete();
        return $data;
    }

    public function deleteBranch($id)
    {

    }
}
