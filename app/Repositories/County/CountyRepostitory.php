<?php

namespace App\Repositories\County;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common\County as county;
use App\Models\Common\Province as province;
use App\Models\Common\City as city;
use DB;

class CountyRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(County $model)
    {
        $this->model=$model;
    }

    public function getAllCounty()
    {
        return County::latest()->paginate(10);
    }

    public function getCounty($id)
    {
       return County::find($id);
    }
    
    public function createCounty($request)
    {
        // return County::create([
        //     'province_id'   =>$request['province_id'],
        //     'city_id'       =>$request['city_id'],
        //     'name'          =>$request['name'],
        // ]);
        $province= new province;
        $city=new city;
        $county=new county;

        $province->name=$request['name'];
        $province->save();
        
    }

    public function updateCounty($request,$id)
    {
        $county = County::find($id);
        $county->update($request->all());

        if($county->save()){
            return true;
        }
        return false;
    }

    public function deleteCounty($id)
    {
        $county =  County::where('id',$id);
        $county->delete();
        return $county;
    }
}