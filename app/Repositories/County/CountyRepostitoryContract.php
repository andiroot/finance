<?php

namespace App\Repositories\County;

interface CountyRepositoryContract
{
    public function getAllCounty();

    public function getCounty($id);
    
    public function createCounty($request);

    public function updateCounty($request,$id);

    public function deleteCounty($id);

}