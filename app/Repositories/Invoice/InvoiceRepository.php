<?php

namespace App\Repositories\Invoice;

use Illuminate\Database\Eloquent\Model;
use App\Models\Income\Customer;
use App\Models\Income\Order;
use App\Models\Income\Invoice;
use App\Traits\RepositoryTrait;
use DB;

class InvoiceRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Invoice $model)
    {
        $this->model = $model;
    }

    public function getAllInvoice()
    {
        //  return Customer::join('provinces','provinces.id','customers.province_id')
        //  ->join('cities','cities.id','customers.city_id')
        //  ->join('counties','counties.id','customers.county_id')
        //  ->select('customers.name',
        //           'customers.id',
        //           'customers.email',
        //           'customers.phone',
        //           'customers.address',
        //           'customers.enabled',
        //           'customers.created_at',
        //           'customers.updated_at',
        //           'customers.deleted_at',
        //           'provinces.name as province_id','cities.name as city_id','counties.name as county_id' 
        //           )->latest()->paginate(10);
        return Invoice::latest()->paginate(10);
    }

    public function getInvoice($id)
    {

       return Invoice::find($id);
        
        // join('provinces','provinces.id','customers.province_id')
        // ->join('cities','cities.id','customers.city_id')
        // ->join('counties','counties.id','customers.county_id')
        // ->select('customers.*','provinces.name as province_id','cities.name as city_id','counties.name as county_id' )
        // ->where('id',$id)
        // ->first();
        

    }

    public function createInvoice($request)
    {
        return Invoice::create([
            'inv_number'=>$request['inv_number'],
            'order_id'=>$request['order_id'],
            'invoiced_at'=>$request['invoiced_at'],
            'due_at'=>$request['due_at'],
            'amount'=>$request['amount'],
            'customer_id'=>$request['customer_id'],
            'notes'=>$request['notes'],
            'attachment'=>$request['attachment'],
        ]);
        
    }

    public function updateInvoice($request,$id)
    {
        $data = Invoice::find($id);
        
        if($data->save()){
            return true;
        }
        return false;

        
    }


    public function deleteInvoice($id)
    {
        $data =  Invoice::where('id',$id);
        $data->delete();
        return $data;
        
    }
}