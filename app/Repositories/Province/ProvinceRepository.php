<?php

namespace App\Repositories\Province;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Province;
use DB;

class ProvinceRepository 
{

    use RepositoryTrait;
    protected $model;

    public function __construct(Province $model)
    {
        $this->model = $model;
    }

    public function getAllProvince()
    {
        return Province::latest()->paginate(10);
    }

    public function getProvince($id)
    {
        return $data = DB::select(
            'select * from provinces 
            where id= :id',['id'=>$id]);
    }
    
    public function createProvince($request)
    {
        // return Province::create([
        //     'name'=>$request['name'],
        // ]);
        $province = new Province;
        $province->name=$request['name'];
        $province->save();
        
    }

    public function updateProvince($request,$id)
    {
        $data = Province::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;
    }

    public function deleteProvince($id)
    {
        $data =  Province::where('id',$id);
        $data->delete();
        return $data;
    }
}