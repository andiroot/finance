<?php

namespace App\Repositories\Province;

interface ProvinceRepositoryContract
{
    public function getAllProvince();

    public function getProvince($id);
    
    public function createProvince($request);

    public function updateProvince($request,$id);

    public function deleteProvince($id);

}