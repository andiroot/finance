<?php

namespace App\Repositories\Uom;

interface UomRepositoryContract
{
    public function getAllUom();

    public function getUom($id);
    
    public function createUom($request);

    public function updateUom($request,$id);

    public function deleteUom($id);

}