<?php

namespace App\Repositories\Uom;

use Illuminate\Database\Eloquent\Model;
use App\Models\Inventory\Uom;
use App\Traits\RepositoryTrait;
use DB;

class UomRepository 
{
    use RepositoryTrait;
    protected $model;

    public function __construct(Uom $model)
    {
        $this->model = $model;
    }

    public function getAllUom()
    {
        return Uom::latest()->paginate(10);
    }

    public function getUom($id)
    {
        return Uom::find($id);
    }

    public function createUom($request)
    {
        return Uom::create([
            'name'       =>$request['name'],
            'description'=>$request['description'],

        ]);
    }

    public function updateUom($request,$id)
    {
        $data = Uom::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;

    }

    public function deleteUom($id)
    {
        $data =  Uom::where('id',$id);
        $data->delete();
        return $data;
    }
}

