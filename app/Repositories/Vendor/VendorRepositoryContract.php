<?php

namespace App\Repositories\Vendor;

interface VendorRepositoryContract
{
    public function getAllVendor();

    public function getVendor($id);
    
    public function createVendor($request);

    public function updateVendor($request,$id);

    public function deleteVendor($id);

}