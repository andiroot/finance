<?php

namespace App\Repositories\Vendor;

use Illuminate\Database\Eloquent\Model;
use App\Models\Expense\Vendor;
use App\Models\Users\User;
use App\Traits\RepositoryTrait;
use Auth;
use DB;

class VendorRepository 
{
    use RepositoryTrait;
    protected $model;

    public function __construct(Vendor $model)
    {
        $this->model = $model;
    }

    public function getAllVendor()
    {
        return Vendor::latest()->paginate(10);
    }

    public function getVendor($id)
    {
        return Vendor::find($id);
    }
    
    public function createVendor($request)
    {
        // $user = $request->user();

        // $request->request->add([
        //     'user_id' => $user->id,
        // ]);
        return Vendor::create([
            'user_id'   =>Auth::id(),
            'name'      => $request['name'],
            'email'     => $request['email'],
            'phone'     => $request['phone'],
            'address'   => $request['address'],
            'enabled'   => $request['enabled'],
        ]);
           
    }
    
    public function updateVendor($request,$id)
    {
        $data = Vendor::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;

    }
    
    public function deleteVendor($id)
    {
        $data =  Vendor::where('id',$id);
        $data->delete();
        return $data;
    }
}
       