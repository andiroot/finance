<?php

namespace App\Repositories\Customer;

use Illuminate\Database\Eloquent\Model;
use App\Models\Income\Customer;
use App\Models\Income\Order;
use App\Models\Common\Courier;
use App\Models\Common\Shipping;
use App\Traits\RepositoryTrait;
use DB;

class ShippingRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Shipping $model)
    {
        $this->model = $model;
    }

    public function getAllShipping()
    {
        //  return Customer::join('provinces','provinces.id','customers.province_id')
        //  ->join('cities','cities.id','customers.city_id')
        //  ->join('counties','counties.id','customers.county_id')
        //  ->select('customers.name',
        //           'customers.id',
        //           'customers.email',
        //           'customers.phone',
        //           'customers.address',
        //           'customers.enabled',
        //           'customers.created_at',
        //           'customers.updated_at',
        //           'customers.deleted_at',
        //           'provinces.name as province_id','cities.name as city_id','counties.name as county_id' 
        //           )->latest()->paginate(10);
        return Shipping::latest()->paginate(10);
    }

    public function getShipping($id)
    {

       return Shipping::find($id);
        
        // join('provinces','provinces.id','customers.province_id')
        // ->join('cities','cities.id','customers.city_id')
        // ->join('counties','counties.id','customers.county_id')
        // ->select('customers.*','provinces.name as province_id','cities.name as city_id','counties.name as county_id' )
        // ->where('id',$id)
        // ->first();
        

    }

    public function createShipping($request)
    {
        return Shipping::create([
            'customer_id'=>$request['customer_id'],
            'order_id'=>$request['order_id'],
            'courier_id'=>$request['courier_id'],
            'type'=>$request['type'],
            'shipping_number'=>$request['shipping_number'],
            'manifest_city'=>$request['manifest_city'],
            'manifest_description'=>$request['manifest_description'],
            'description'=>$request['description'],
            'notes'=>$request['notes'],
            'status'=>$request['status'],
        ]);
        
    }

    public function updateShipping($request,$id)
    {
        $data = Shipping::find($id);
        
        if($data->save()){
            return true;
        }
        return false;

        
    }


    public function deleteShipping($id)
    {
        $data =  Shipping::where('id',$id);
        $data->delete();
        return $data;
        
    }
}