<?php

namespace App\Repositories\Payment;

interface PaymentRepositoryContract
{
    public function getAllPayment();

    public function getPayment($id);
    
    public function createPayment($request);

    public function updatePayment($request,$id);

    public function deletePayment($id);

}