<?php

namespace App\Repositories\Payment;

use Illuminate\Database\Eloquent\Model;
use App\Models\Expense\Payment;
use App\Traits\RepositoryTrait;
use DB;

class PaymentRepository 
{
    use RepositoryTrait;
    protected $model;

    public function __construct(Payment $model)
    {
        $this->model = $model;
    }

    public function getAllPayment()
    {
        // return Payment::join('accounts','accounts.id','payment.account_id')
        // ->join('vendors','vendors.id','payment.vendor_id')
        // ->select('payments.*','vendors.name as vendor_id','accounts.name as account_id')->get();
        return Payment::latest()->paginate(10);
    }

    public function getPayment($id)
    {
        return Payment::find($id);
    }

    public function createPayment($request)
    {
        return Payment::create([
            'account_id'        =>$request['account_id'],
            'paid_at'           =>$request['paid_at'],
            'amount'            =>$request['amount'],
            'vendor_id'         =>$request['vendor_id'],
            'description'       =>$request['description'],
        ]);
    }

    public function updatePayment($request,$id)
    {
        $data = Payment::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;
    }

    public function deletePayment($id)
    {
        $data =  Payment::where('id',$id);
        $data->delete();
        return $data;
    }

}
