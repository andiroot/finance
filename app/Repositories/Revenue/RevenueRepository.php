<?php

namespace App\Repositories\Revenue;

use Illuminate\Database\Eloquent\Model;
use App\Models\Income\Revenue;
use App\Models\Banking\Account;
use App\Models\Common\Customer;
use App\Traits\RepositoryTrait;
use DB;

class RevenuesRepository 
{
    use RepositoryTrait;
    protected $model;

    public function __construct(Revenue $model)
    {
        $this->model = $model;
    }

    public function getAllRevenues()
    {
        return Revenue::latest()->paginate(10);
    }

    public function getRevenues($id)
    {
        return Revenue::find($id);
    }
    
    public function createRevenues($request){
        return Revenue::create([
            'account_id'    =>$request['account_id'],
            'paid_at'       =>$request['paid_at'],
            'amount'        =>$request['amount'],
            'customer_id'   =>$request['customer_id'],
            'description'   =>$request['description'],
            'payment_method'=>$request['payment_method'],
            'references'    =>$request['references'],
            'attachment'    =>$request['attachment'],
        ]);
    
    }
    public function updateRevenues($request,$id)
    {
        $data = Revenue::find($id);
        $data->update($request->all());
        if($data->save()){
            return true;
        }
        return false;
    }
    
    public function deleteRevenues($id)
    {
        $data =  Revenue::where('id',$id);
        $data->delete();
        return $data;
    }
}

