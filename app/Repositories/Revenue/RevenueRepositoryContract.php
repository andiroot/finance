<?php

namespace App\Repositories\Revenues;

interface RevenuesRepositoryContract
{
    public function getAllRevenues();

    public function getRevenues($id);
    
    public function createRevenues($request);

    public function updateRevenues($request,$id);

    public function deleteRevenues($id);

}