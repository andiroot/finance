<?php

namespace App\Repositories\Loan;

use Illuminate\Database\Eloquent\Model;
use App\Models\Payroll\Loan;
use App\Models\Payroll\Employee;
use App\Traits\RepositoryTrait;
use DB;

class LoanRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(Loan $model)
    {
        $this->model = $model;
    }

    public function getAllLoan()
    {
        //  return Customer::join('provinces','provinces.id','customers.province_id')
        //  ->join('cities','cities.id','customers.city_id')
        //  ->join('counties','counties.id','customers.county_id')
        //  ->select('customers.name',
        //           'customers.id',
        //           'customers.email',
        //           'customers.phone',
        //           'customers.address',
        //           'customers.enabled',
        //           'customers.created_at',
        //           'customers.updated_at',
        //           'customers.deleted_at',
        //           'provinces.name as province_id','cities.name as city_id','counties.name as county_id' 
        //           )->latest()->paginate(10);
        return Loan::latest()->paginate(10);
    }

    public function getLoan($id)
    {

       return Loan::find($id);
        
        // join('provinces','provinces.id','customers.province_id')
        // ->join('cities','cities.id','customers.city_id')
        // ->join('counties','counties.id','customers.county_id')
        // ->select('customers.*','provinces.name as province_id','cities.name as city_id','counties.name as county_id' )
        // ->where('id',$id)
        // ->first();
        

    }

    public function createLoan($request)
    {
        return Loan::create([
            'employee_id'=>$request['employee_id'],
            'name'=>$request['name'],
            'amount'=>$request['amount'],
            'loandate'=>$request['loandate'],
        ]);
        
    }

    public function updateLoan($request,$id)
    {
        $data = Loan::find($id);
        
        if($data->save()){
            return true;
        }
        return false;

        
    }


    public function deleteLoan($id)
    {
        $data =  Loan::where('id',$id);
        $data->delete();
        return $data;
        
    }
}