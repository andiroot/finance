<?php

namespace App\Repositories\City;

interface CityRepositoryContract
{
    public function getAllCity();

    public function getCity($id);
    
    public function createCity($request);

    public function updateCity($request,$id);

    public function deleteCity($id);

}