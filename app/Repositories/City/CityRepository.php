<?php

namespace App\Repositories\City;

use App\Traits\RepositoryTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Province as province;
use App\Models\Common\City as city;
use DB;

class CityRepository 
{
    use RepositoryTrait;

    protected $model;

    public function __construct(City $model)
    {
        $this->model=$model;
    }

    public function getAllCity()
    {   
        return City::latest()->paginate(10);
        
    }

    public function getCity($id)
    {
        $data = City::find($id);
        return $data;
    }
    
    public function createCity($request)
    {
        // return City::create([
        //     'province_id'   =>$request['province_id'],
        //     'name'          =>$request['name'],
        // ]);
        $province=new province;
        $city=new city;
        $province=province::find(21);
        $city->name=$request['name'];
        $city->province_id=$request['province_id'];
        $city->push();

        // $city->province->dissociate();
        // $city->save();


    }

    public function updateCity($request,$id)
    {
        $city = City::find($id);
        $city->update($request->all());

        if($if->save()){
            return true;
        }
        return false;
    }

    public function deleteCity($id)
    {
        $city = City::where('id',$id);
        $city->delete();
        
        return $city;
    }
}
