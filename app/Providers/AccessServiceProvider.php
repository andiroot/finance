<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AccessServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(

            \App\Repositories\Account\AccountRepositoryContract::class,
            \App\Repositories\Account\AccountRepository::class,
            \App\Repositories\Transaction\TransactionRepository::class,
            \App\Repositories\Transaction\TransactionRepositoryContract::class,
            
            
            
            // \App\Repositories\Bill\BillRepository::class,
            // \App\Repositories\Bill\BillRepositoryContract::class,
            // \App\Repositories\Branch\BranchRepository::class,
            // \App\Repositories\Branch\BranchRepositoryContract::class,
            \App\Repositories\Category\CategoryRepository::class,
            \App\Repositories\Category\CategoryRepositoryContract::class,
            \App\Repositories\City\CityRepository::class,
            \App\Repositories\City\CityRepositoryContract::class,
            \App\Repositories\Contact\ContactRepository::class,
            \App\Repositories\Contact\ContactRepositoryContract::class,
            \App\Repositories\County\CountyRepository::class,
            \App\Repositories\County\CountyRepositoryContract::class,
            // \App\Repositories\Courier\CourierRepository::class,
            // \App\Repositories\Courier\CourierRepositoryContract::class,
            // \App\Repositories\CourierCoverage\CourierCoverageRepository::class,
            // \App\Repositories\CourierCoverage\CourierCoverageRepositoryContract::class,
            \App\Repositories\Customer\CustomerRepositoryContract::class,
            \App\Repositories\Customer\CustomerRepository::class,
            \App\Repositories\Department\DepartmentRepository::class,
            \App\Repositories\Department\DepartmentRepositoryContract::class,
            \App\Repositories\Designation\DesignationRepository::class,
            \App\Repositories\Designation\DesignaitonRepositoryContract::class,
            \App\Repositories\Employee\EmployeeRepository::class,
            \App\Repositories\Employee\EmployeeRepositoryContract::class,
            \App\Repositories\Inventory\InventoryRepository::class,
            \App\Repositories\Inventory\InventoryRepositoryContract::class,
            \App\Repositories\Invoice\InvoiceRepository::class,
            \App\Repositories\Invoice\InvoiceRepositoryContract::class,
            \App\Repositories\Loan\LoanRepository::class,
            \App\Repositories\Loan\LoanRespositoryContruct::class,
            \App\Repositories\Order\OrderRepository::class,
            \App\Repositories\Order\OrderRepositoryContruct::class,
            \App\Repositories\Package\PackageRepository::class,
            \App\Repositories\Package\PackageRepositoryContruct::class,
            \App\Repositories\Payment\PaymentRepository::class,
            \App\Repositories\Payment\PaymentRepositoryContruct::class,
            \App\Repositories\Product\ProductRepository::class,
            \App\Repositories\Product\ProductRepositoryContruct::class,
            \App\Repositories\Province\ProvinceRepository::class,
            \App\Repositories\Province\ProvinceRepostitoryContruct::class,
            \App\Repositories\Reconciliation\ReconciliationRepository::class,
            \App\Repositories\Reconciliation\ReconciliationRepositoryContruct::class,
            \App\Repositories\Report\ReportRepository::class,
            \App\Repositories\Report\ReportRepositoryContruct::class,
            \App\Repositories\Revenue\RevenueRepository::class,
            \App\Repositories\Revenue\RevenueRepositoryContract::class,
            \App\Repositories\Shipping\ShippingRepository::class,
            \App\Repositories\Shipping\ShippingRepositoryContruct::class,
            \App\Repositories\ShippingOrder\ShippingOrderRepository::class,
            \App\Repositories\ShippingOrder\ShippingOrderRepositoryContruct::class,
            \App\Repositories\Tax\TaxRepository::class,
            \App\Repositories\Tax\TaxRepositoryContruct::class,
            
            \App\Repositories\Transfer\TransferRepository::class,            
            \App\Repositories\Transfer\TransferRepositoryContract::class,
            \App\Repositories\Uom\UomRepository::class,
            \App\Repositories\Uom\UomRepositoryContruct::class,
            \App\Repositories\Users\UsersRepository::class,
            \App\Repositories\Users\UsersRepositoryContruct::class,
            \App\Repositories\Vendor\VendorRepository::class,
            \App\Repositories\Vendor\VendorRepositoryContruct::class,
            \App\Repositories\Warehouse\WarehouseRepository::class,
            \App\Repositories\Warehouse\WarehouseRepositoryContruct::class,



            

        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
