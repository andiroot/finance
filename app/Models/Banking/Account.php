<?php

namespace App\Models\Banking;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table='accounts';
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    //protected $appends = ['balance'];

    protected $fillable=[
        'name',
        'bank_number',
        'bank_name',
        'bank_phone',
        'bank_address',
        'opening_balance',
        'type',
        'enabled',
    ];

    public function payment()
    {
        return $this->hasMany('App\Models\Expense\Payment');
    }
    
    public function reconciliation()
    {
        return $this->hasMany('App\Models\Common\Reconciliation');
    }

    public function revenue()
    {
        return $this->hasMany('App\Models\Income\Revenue');
    }

    public function bill_payment()
    {
        return $this->hasMany('App\Models\Expense\Bill_Payment');
    }

    public function invoice_payment()
    {
        return $this->hasMany('App\Models\Income\Invoice_Payment');
    }
    public function transactions()
    {
        return $this->hasMany('App\Models\Banking\Transaction');
    }


    /**
     * implement Accessor
     */
    public function getEnabledAttribute($value)
    {
        if($value){
            return $value = "Active";
        }else{
            return $value = "NonActive";
        }
        return $value;
    }

    /**
     * Implement Mutator
     */
    public function setTypeAttribute($value)
    {
        $this->attributes['type']=ucfirst($value);
    }

        /**
     * Convert opening balance to double.
     *
     * @param  string  $value
     * @return void
     */
    public function setOpeningBalanceAttribute($value)
    {
        $this->attributes['opening_balance'] = (double) $value;
    }
}
