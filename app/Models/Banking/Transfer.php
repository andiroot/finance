<?php

namespace App\Models\Banking;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $table='transfers';

    protected $fillable=[
        'payment_id',
        'revenue_id',
    ];

    protected $sortable='created_at';

    public function payment()
    {
        return $this->belongsTo('App\Models\Expense\Payment');
    }

    public function revenue()
    {
        return $this->belongsTo('App\Models\Income\Revenue');
    }
}
