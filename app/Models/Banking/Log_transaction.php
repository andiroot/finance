<?php

namespace App\Models\Banking;

use Illuminate\Database\Eloquent\Model;

class Log_transaction extends Model
{
    protected $table='log_transactions';

    protected $fillable=[
        'user_id',
        'transaction_id',
        'description',
        'transaction_type',
        'transaction_date',
        'saldo',
    ];

    protected $sortable='user_id';

    public function transaction()
    {
        return $this->belongsTo('App\Models\Banking\Transaction');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
}
