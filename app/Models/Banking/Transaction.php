<?php

namespace App\Models\Banking;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table='transactions';

    protected $fillable=[
        'type',
        'account_id',
        'paid_at',
        'amount',
        'currency_code',
        'currency_rate',
        'contact_id',
        'description',
        'category_id',
        'payment_method',
        'references',
        'reconciled'
    ];  

    public function account()
    {
        return $this->belongsTo('App\Models\Banking\Account')->withDefault(['name' => trans('general.na')]);
    }

    public function contact()
    {
        return $this->belongsTo('App\Models\Common\Contact');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Invetory\Category');
    }

    public function Log_transaction()
    {
        return $this->hasMany('App\Models\Bangking\Log_transaction');
    }

        /**
     * Convert amount to double.
     *
     * @param  string  $value
     * @return void
     */
    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (double) $value;
    }
}
