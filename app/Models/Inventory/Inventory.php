<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table='inventories';

    protected $fillable=[
        'product_id',
        'customer_id',
        'vendor_id',
        'type',
        'qty',
        'tran_date',
        'status',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Inventory\Product');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Income\Customer');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Expense\Vendor');
    }
}
