<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categories';

    protected $fillable=[
        'type',
        'name',
        'color',
        'enabled',
    ];
    
    protected $sortable='name';

    public function product()
    {
        return $this->hasMany('App\Models\Inventory\Product');
    }

    public function transaction()
    {
        return $this->hasMany('App\Models\Banking\Transaction');
    }
}
