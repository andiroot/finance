<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Product_Photo extends Model
{
    protected $table='product_photos';

    protected $fillable=[
        'product_id',
        'photo',
        'enabled',
    ];

    protected $sortable='product_id';

    
}
