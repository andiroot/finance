<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    protected $table='uoms';

    protected $fillable=[
        'name',
        'description',
    ];

    protected $sortable='name';

    public function product()
    {
        return $this->hasMany('App\Models\Inventory\Product');
    }
}
