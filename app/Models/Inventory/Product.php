<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';

    protected $fillable=[

        'name',
        'sku',
        'description',
        'sale_price',
        'purchase_price',
        'capital_price',
        'qty',
        'vendor_id',
        'category_id',
        'uom_id',
        'tax_id',
        'weight',
        'price_closing',
        'enabled',
    ];

    protected $sortable='created_at';

    public function package()
    {
        return $this->hasMany('App\Models\Inventory\Package');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\Common\Tax');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Expense\Vendor');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Inventory\Category');
    }

    public function uom()
    {
        return $this->belongsTo('App\Models\Inventory\Uom');
    }

    public function warehouse()
    {
        return $this->belongsToMany(Warehouse::class,'warehouse_id');
    }

    public function order(){
        return $this->belongsToMany(Order::class,'order_id');
    }

    public function bill_product()
    {
        return $this->hasMany('App\Models\Expense\Bill_Product');
    }

    public function invoice_product()
    {
        return $this->hasMany('App\Models\Income\Invoice');
    }

    public function inventory()
    {
        return $this->hasMany('App\Model\Inventory\Inventory');
    }
}

