<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table='warehouses';

    protected $fillable=[
        'name',
        'province_id',
        'city_id',
        'county_id',
        'address',
        'product_id',
        'package_id',
    ];

    protected $sortable='address';

    public function province()
    {
        return $this->belongsTo('App\Models\Common\Province');
    }
    
    public function city()
    {
        return $this->belongsTo('App\Models\Common\City');
    }

    public function county()
    {
        return $this->belongsTo('App\Models\Common\County');
    }

    public function product()
    {
        return $this->belongsTomany( Product::class,'product_warehouses');
    }

    public function package()
    {
        return $this->belongsTo('App\Models\Inventory\Package');
    }

}
