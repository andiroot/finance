<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table='packages';
    
    protected $fillable=[
        'name',
        'sales_price',
        'purchase_price',
        'product_id',
        'qty'
    ];

    protected $sortable='name';

    public function warehouse()
    {
        return $this->hasMany('App\Models\Inventory\Warehouse');
    }
    
    public function product()
    {
        return $this->belongsToMany(Product::class,'product_packages');
    }

    /**
     *
     * public function warehouse()
     * {
     *   return $this->hasMany('App\Models\Inventory\Warehouse');     
     * }
     */
    
}
