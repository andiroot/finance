<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Product_Package extends Model
{
    protected $table='product_packages';

    protected $fillable=[
        'package_id',
        'product_id',
        'photo',
        'enabled',
    ];
}
