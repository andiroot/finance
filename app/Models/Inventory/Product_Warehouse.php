<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Product_Warehouse extends Model
{   
    protected $table='product_warehouses';
    
    protected $fillable=[
        'warehouse_id',
        'product_id',
        'package_id',
        'stock',
        'enabled',
    ];

    protected $sortable='warehouse_id';

}
