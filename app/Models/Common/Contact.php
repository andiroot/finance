<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table='contacts';

    protected $fillable=[
        'name',
        'type',
        'email',
        'phone',
        'address',
        'enable',
    ];

    protected $sortable='name';

    public function transaction()
    {
        return $this->hasMany('App\Models\Banking\Transaction');
    }   

    
}
