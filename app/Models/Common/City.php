<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table='cities';

    //Attribute assignable
    protected $fillable=[
        'province_id',
        'name',
    ];

    //Sortable column
    public $sortable = [
        'province_id'
    ];

    public function province()
    {
        return $this->belongsTo('App\Models\Common\Province');
    }

    public function courier_coverage()
    {
        return $this->hasMany('App\Models\Common\Courier_coverage');
    }
    
    public function customer()
    {
        return $this->hasMany('App\Models\Income\Customer');
    }

    public function warehouse()
    {
        return $this->hasMany('App\Models\Inventory\Warehouse');
    }
}
