<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Courier_Coverage extends Model
{
    protected $table='courier_coverages';

    protected $fillable=[
        'courier_id',
        'province_id',
        'city_id',
        'county_id',
        'neighbourhood',
        'estimation',
        'postal_code',
        'sla',
        'price',
        'service',
        'tax_id',
    ];

    protected $sortable='courier_id';

    public function courier()
    {
       return $this->belongsTo('App\Models\Common\Courier');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Common\Province');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Common\City');
    }

    public function county()
    {
        return $this->belongsTo('App\Models\Common\County');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\Common\Tax');
    }
}
