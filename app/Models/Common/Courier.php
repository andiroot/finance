<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $table='couriers';

    protected $fillable=[
        'name',
        'address',
        'company',
        'phone',
        // 'tax_id',
    ];

    protected $sortable='name';

    public function tax()
    {
       return $this->belongsTo('App\Models\Common\Tax');
    }
    
    public function courier_coverage()
    {
        return $this->hasMany('App\Models\Common\Courier_coverage');
    }
}
