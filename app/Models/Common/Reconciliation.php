<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Reconciliation extends Model
{
    protected $table='reconciliations';

    protected $fillable=[
        'account_id',
        'started_at',
        'ended_at',
        'closing_balance',
        'reconciled',
    ];

    protected $sortable='account_id';

    public function account()
    {
        return $this->belongsTo('App\Models\Banking\Account');
    }
}
