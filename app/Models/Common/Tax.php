<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $table='taxes';
    
    protected $fillable=[
        'name',
        'rate',
        'enabled',
    ];

    protected $sortable='name';

    public function courier()
    {
       return $this->hasMany('App\Models\Common\Courier');
    }

    public function Courier_coverage()
    {
        return $this->hasMany('App\Models\Common\Courier_coverage');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Inventory\Product');
    }

    public function bill_product()
    {
        return $this->hasMany('App\Models\Expense\Bill_Product');
    }

    public function bill_product_tax()
    {
        return $this->hasMany('App\Models\Expense\Bill_Product_Tax');
    }

    public function invoice_product()
    {
        return $this->hasMany('App\Models\Income\Invoice_Product');
    }

    public function invoice_product_tax()
    {
        return $this->hasMany('App\Models\Income\Invoice_Product_tax');
    }


}
