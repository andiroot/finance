<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $table='counties';

    protected $fillable=[
        'province_id',
        'city_id',
        'name',
    ];

    protected $sortable='name';

    public function province()
    {
        return $this->belongsTo('App\Models\Common\Province');
    }
    
    public function city()
    {
        return $this->hasMany('App\Models\Common\City');
    }

    public function courier_coverage()
    {
        return $this->hasMany('App\Models\Common\Courier_coverage');
    }

    public function customer()
    {
        return $this->hasMany('App\Models\Income\Customer');
    }

    public function warehouse()
    {
        return $this->hasMany('App\Models\Inventory\Warehouse');
    }
}
