<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    public $table = "deals";
    protected $fillable = [
        'reportdate'      ,
        'name'     ,
        'lead',
        'closing'    ,
        'payment_method',
        'type',
        'notes'    
        
    ];      
    

}