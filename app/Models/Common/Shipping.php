<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $table='shippings';

    protected $fillable=[
        'customer_id',
        'order_id',
        'courier_id',
        'type',
        'shipping_number',
        'manifest_city',
        'manifest_description',
        'description',
        'notes',
        'status',
    ];

    protected $sortable='courier_id';

    public function customer()
    {
        return $this->belongsTo('App\Models\Income\Customer');
    }

    public function order()
    {
        return $this->belongsToMany(Order::class,'order_id');
    }

    public function Courier()
    {
        return $this->belongsTo('App\Models\Common\Courier');
    }


}
