<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Shipping_Orders extends Model
{
    protected $table='shipping_orders';

    protected $fillable=[
        'shipping_id',
        'order_id',
        'qty',
    ];

    protected $sortable='shipping_id';

    
}
