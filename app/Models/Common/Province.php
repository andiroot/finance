<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table='provinces';

    protected $fillable=[
        'name'
    ];

    protected $sortable='name';

    public function city()
    {
        return $this->hasMany('App\Models\Common\City');
    }

    public function county()
    {
        return $this->hasMany('App\Models\Common\County');
    }

    public function courier_coverage()
    {
        return $this->hasMany('App\Models\Common\Courier_coverage');
    }

    public function customer()
    {
        return $this->hasMany('App\Models\Income\Customer');
    }

    public function warehouse()
    {
        return $this->hasMany('App\Models\Inventory\Warehouse');
    }
}
