<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Invoice_Status extends Model
{
    protected $table='invoice_statuses';

    protected $fillable=[
        'name',
        'code',
    ];

}
