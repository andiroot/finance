<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table='invoices';

    protected $fillable=[
        'inv_number',
        'order_id',
        'invoiced_at',
        'due_at',
        'amount',
        'customer_id',
        'notes',
        'attachment',
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Income\Order');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Income\Customer');
    }

    public function invoice_product()
    {
        return $this->hasMany('App\Models\Income\Invoice_Product');
    }

    public function invoice_payment()
    {
        return $this->belongsTo('App\Models\Invomce\Invoice_Payment');
    }

    public function invoice_history()
    {
        return $this->hasMany('App\Models\Income\Invoice_History');
    }

    public function invoice_totals()
    {
        return $this->hasMany('App\Models\Income\Invoice_Totals');
    }

    public function invoice_product_taxes()
    {
        return $this->hasMany('App\Models\Income\Invoice_Product_taxes');
    }
}
