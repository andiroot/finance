<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    protected $table='revenues';

    protected $fillable=[
        'account_id',
        'paid_at',
        'amount',
        'customer_id',
        'description',
        'payment_method',
        'references',
        'attachment',
    ];

    protected $sortable='account_id';

    public function account()
    {
        return $this->belongsTo('App\Models\Banking\Account');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Income\Customer');
    }

    public function transfer()
    {
        return $this->hasMany('App\Models\Banking\Transfer');
    }
}
