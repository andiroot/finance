<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Invoice_Totals extends Model
{
    protected $table='invoice_totals';

    protected $fillable=[
        'invoice_id',
        'code',
        'name',
        'amount',
        'sort_order',
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Income\Invoice');
    }

}
