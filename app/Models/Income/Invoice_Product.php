<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Invoice_Product extends Model
{
    protected $table='invoice_products';

    protected $fillable=[
        'invoice_id',
        'product_id',
        'qty',
        'total',
        'tax_id',
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Income\Invoice');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Inventory\Product');
    }

    public function tax_id()
    {
        return $this->belongsTo('App\Models\Common\Tax');
    }

    public function invoice_product_tax()
    {
        return $this->hasMany('App\Models\Income\Invoice_Product_tax');
    }
}
