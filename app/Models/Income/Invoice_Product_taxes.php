<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Invoice_Product_taxes extends Model
{
    protected $table='invoice_product_taxes';

    protected $fillable=[
        'invoice_id',
        'invoice_product_id',
        'tax_id',
        'amount',
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Income\Invoice');
    }

    public function invoice_product()
    {
        return $this->belongsTo('App\Models\Invome\Invoice_Product');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\Common\Tax');
    }
}
