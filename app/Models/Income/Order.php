<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";

    protected $fillable = [
           'order_number',
           'customer_id',
           'employee_id',
           'product_id',
           'payment_method',
           'status',
           'net_profit',
           'processed_at',
           'processed_by',
    ];
    

    
    public function employee()
    {
        return $this->belongsTo('App\Models\Payroll\Employee');
    }

    public function product()
    {
        return $this->belongsToMany(Product::class,'product_id');
    }

    public function shipping()
    {
        return $this->belongsToMany(Shipping::class,'shipping_id');
    }


    public function bill()
    {
        return $this->hasMany('App\Models\Expense\Bill');
    }

    public function invoice()
    {
        return $this->hasMany('App\Models\Income\Invoice');
    }
}
