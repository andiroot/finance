<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Order_Detail extends Model
{
    protected $table='order_details';

    protected $fillable=[
        'product_id',
        'order_id',
        'shipping_id',
        'qty',
    ];

    public function Shipping_Order()
    {
        return $this->hasMany('App\Models\Common\Shipping_Order');
    }
}
