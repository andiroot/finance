<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Invoice_History extends Model
{
    protected $table='invoice_histories';

    protected $fillable=[
        'invoice_id',
        'status_code',
        'notify',
        'description',
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Income\Invoice');
    }

}
