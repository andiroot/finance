<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table='customers';

    protected $fillable=[
        'name',
        'email',
        'address',
        'phone',
        'province_id',
        'city_id',
        'county_id',
        'enabled',
    ];

    protected $sortable='name';

    public function province()
    {
        return $this->belongsTo('App\Models\Common\Province');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Common\City');
    }

    public function county()
    {
        return $this->belongsTo('App\Models\Common\County');
    }

    public function revenue()
    {
        return $this->hasMany('App\Models\Income\Revenue');
    }

    public function shipping()
    {
        return $this->hasMany('App\Models\Common\Shipping');
    }

    public function invoice()
    {
        return $this->hasMany('App\Models\Income\Invoice');
    }

    public function inventory()
    {
        return $this->hasMany('App\Models\Inventory\Inventory');
    }

    /**
     * implement Accessor
     */
    public function getEnabledAttribute($value)
    {
        if($value){

            return 'Active'; 
            //'<span class="badge bg-success">Enable</span>';

        }else{
            return 'Active';
            //'<span class="badge bg-danger">Disable</span>';
        }
        return $value;
    }   
}
