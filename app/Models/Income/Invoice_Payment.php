<?php

namespace App\Models\Income;

use Illuminate\Database\Eloquent\Model;

class Invoice_Payment extends Model
{
    protected $table='invoice_payments';

    protected $fillable=[
        'invoice_id',
        'account_id',
        'paid_at',
        'amount',
        'description',
        'payment_method',
        'references',
        'attachment'
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Income\Invoice');
    }

    public function account()
    {
        return $this->belongsTo('App\Model\Banking\Account');
    }

}
