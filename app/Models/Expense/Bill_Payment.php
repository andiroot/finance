<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Bill_Payment extends Model
{
    protected $table='bill_payments';

    protected $fillable=[
        'bill_id',
        'account_id',
        'paid_at',
        'amount',
        'description',
        'payment_method',
        'references',
        'attachment',
    ];

    protected $sortable='bill_id';

    public function bill()
    {
        return $this->belongsTo('App\Models\Expense\Bill');
    }

    public function account()
    {
        return $this->belongsTo('App\Models\Banking\Account');
    }
    
}
