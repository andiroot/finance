<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table='bills';

    protected $fillable=[
        'bill_number',
        'order_id',
        'bill_status_code',
        'billed_at',
        'due_at',
        'amount',
        'vendor_id',
        'notes',
        'attachment',
    ];

    protected $sortable='created_at';

    public function order()
    {
        return $this->belongsTo('App\Models\Income\Oder');
    }
    
    public function bill_product()
    {
        return $this->hasMany('App\Models\Expense\Bill_Product');
    }

    public function bill_payment()
    {
        return $this->hasMany('App\Models\Expense\Bill_Payment');
    }

    public function bill_history()
    {
        return $this->hasMany('App\Models\Expense\Bill_History');
    }

    public function bill_totals()
    {
        return $this->hasMany('App\Models\Expense\Bill_Total');
    }

    public function bill_product_tax()
    {
        return $this->hasMany('App\Models\Expense\Bill_Product_Tax');
    }
}
