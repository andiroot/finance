<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Bill_Product extends Model
{
    protected $table='bill_products';

    protected $fillable=[
        'bill_id',
        'product_id',
        'qty',
        'total',
        'tax_id',
    ];

    protected $sortable='bill_id';

    public function bill()
    {
        return $this->belongsTo('App\Models\Expense\Bill');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Inventory\Product');
    }

    public function tax()
    {
        return $this->belongsTo('App\Models\Common\Tax');
    }

    public function bill_product_tax()
    {

        return $this-hasMany('App\Models\Expense\Bill_Product_Tax');

    }

    
}
