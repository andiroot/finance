<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Bill_Total extends Model
{
    protected $table='bill_totals';

    protected $fillable=[
        'bill_id',
        'code',
        'name',
        'amount',
        'sort_order',
    ];

    public function bill()
    {
        return $this->belongsTo('App\Models\Expense\Bill');
    }
}
