<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table='payments';

    protected $fillable=[
        'account_id',
        'paid_at',
        'amount',
        'vendor_id',
        'description',
    ];  

    protected $sortable='paid_at';

    public function account()
    {
        return $this->belongsTo('App\Models\Banking\Account');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Expense\Vendor');
    }
    
    public function transfer()
    {
        return $this->hasMany('App\Models\Expense\Payment');
    }
}
