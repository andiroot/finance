<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table='vendors';

    protected $fillable=[
        'user_id',
        'name',
        'email',
        'tax_number',
        'phone',
        'address',
        'website',
        'enabled',
    ];

    protected $sortable='name';

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User');
    }

    public function product()
    {
        return $this->hasMany('App\Models\Inventory\Product');
    }

    public function payment()
    {
        return $this->hasMany('App\Models\Expense\Payment');
    }

    public function inventory()
    {
        return $this->hasMany('App\Models\Inventory\Inventory');
    }
}
