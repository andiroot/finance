<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Bill_History extends Model
{
    protected $table='bill_histories';
    
    protected $fillable=[
        'bill_id',
        'status_code',
        'notify',
        'description',
    ];

    public function bill()
    {
        return $this->belongsTo('App\Models\Expense\Bill');
    }
}
