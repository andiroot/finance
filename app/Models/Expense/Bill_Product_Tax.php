<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Bill_Product_Tax extends Model
{
    protected $table='bill_product_taxes';

    protected $fillable=[
        'bill_id',
        'bill_product_id',
        'tax_id',
        'amount',
    ];

    public function bill()
    {
        return $this->belongsTo('App\Models\Expense\Bill');
    }

    public function bill_product()
    {
        return $this->belongsTo('App\Models\Expense\Bill_Product');
    }
    
    public function tax()
    {
        return $this->belongsTo('App\Models\Common\Tax');
    }
}
