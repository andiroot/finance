<?php

namespace App\Models\Expense;

use Illuminate\Database\Eloquent\Model;

class Bill_Status extends Model
{
    protected $table='bill_statuses';

    protected $fillable=[
        'name',
        'code',
    ];
    
}
