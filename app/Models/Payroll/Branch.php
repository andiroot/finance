<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table='branch';

    protected $fillable=[
        'name',
        'address',
        'province_id',
        'city_id',
        'county_id',
        'enabled',
    ];

}