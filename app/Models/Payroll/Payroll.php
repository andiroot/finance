<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    protected $table='payrolls';

    protected $fillable=[
        'employee_id',
        'pay_date',
        'branch_id',
        'department_id',
        'attendance_id',
        'loan_id',
        'deduction',
        'bonus',
    ];
}
