<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table='attendance';

    protected $fillable=[
        'session_id',
        'employee_id',
        'latitude',
        'longitude',
    ];
}
