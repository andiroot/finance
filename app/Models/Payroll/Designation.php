<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table='designations';

    protected $fillable=[
        'name',
        'enabled',
    ];
}
