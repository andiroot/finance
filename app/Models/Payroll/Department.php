<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table='departments';

    protected $fillable=[
        'name',
        'base_salary',
        'base_overtime',
        'allowance',
        'enabled',
    ];
}
