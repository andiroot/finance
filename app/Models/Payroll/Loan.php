<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $table='loans';

    protected $fillable=[
        'employee_id',
        'name',
        'amount',
        'loandate',
    ];
}
