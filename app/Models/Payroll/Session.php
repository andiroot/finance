<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table='sessions';

    protected $fillable=[
        'user_id',
        'shift_type',
        'shift_start',
        'shift_end',
    ];
}
