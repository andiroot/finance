<?php

namespace App\Models\Payroll;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table='employees';

    protected $fillable=[
        'branch_id',
        'user_id',
        'department_id',
        'designation_id',
        'account_id',
        'nip',
        'address',
        'phone',
        'province_id',
        'city_id',
        'county_id',
    ];

    public function order()
    {
        return $this->hasMany('App\Model\Income\Order');
    }
}
