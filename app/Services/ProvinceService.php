<?php
namespace App\Services;

use App\Repositories\Province\ProvinceRepository;
use App\Models\Common\Province;


class ProvinceService
{
    protected $provinceRepo;

    public function __construct(ProvinceRepository $provinceRepo)
    {
        $this->provinceRepo = $provinceRepo;
    }

    public function getAllProvince()
    {
        $data = $this->provinceRepo->getAllProvince();
        return $data;
    }

    public function getProvince($id)
    {
        $data = $this->provinceRepo->getById($id);
        return $data;
    }
    
    public function createProvince($request)
    {
        $this->provinceRepo->createProvince($request);
        return true;
    }

    public function updateProvince($request,$id)
    {
        $this->provinceRepo->update($request->all(),$id);
        return true;
    }

    public function deleteProvince($id)
    {
        $this->provinceRepo->destroy($this->provinceRepo->getById($id));
        return true;
    }
}