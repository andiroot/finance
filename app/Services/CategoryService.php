<?php
namespace App\Services;

use App\Repositories\Category\CategoryRepository;
use App\Models\Common\Category;


class CategoryService
{
    protected $categoryRepo;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    public function getAllCategory()
    {
        $data = $this->categoryRepo->getAllCategory();
        return $data;
    }
    
    public function getCategory($id)
    {
        $data = $this->categoryRepo->getById($id);
        return $data;
    }

    public function createCategory($request)
    {
        $this->categoryRepo->store($request->all());
        return true;
    }

    public function updateCategory($request,$id)
    {
        $this->categoryRepo->update($request->all(),$id);
        return true;
    }

    public function deleteCategory($id)
    {
        $this->categoryRepo->destroy($this->categoryRepo->getById());
        return true;
    }
}
