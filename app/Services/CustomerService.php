<?php
namespace App\Services;

use App\Repositories\Customer\CustomerRepository;
use App\Models\Income\Customer;


class CustomerService
{
    protected $customerRepo;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }

    public function getAllCustomer()
    {
        $data = $this->customerRepo->getAllCustomer();
        return $data;
    }

    public function getCustomer($id)
    {
        $data = $this->customerRepo->getCustomer($id);
        return $data;
    }

    public function createCustomer($request)
    {
        $this->customerRepo->store($request);
        
        return true;
    }

    public function updateCustomer($request,$id)
    {
        $this->customerRepo->update($request,$id);
        return true;
    }

    public function deleteCustomer($id)
    {
        $this->customerRepo->destroy($this->customerRepo->getById($id));
        return true;
    }
}