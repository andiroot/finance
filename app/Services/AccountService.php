<?php
namespace App\Services;

use App\Repositories\Account\AccountRepository;
use App\Models\Banking\Account;


class AccountService
{
    protected $accountRepo;

    public function __construct(AccountRepository $accountRepo)
    {
        $this->accountRepo = $accountRepo;
    }

    //using trait
    public function getAll()
    {
        $data = $this->accountRepo->all();
        return $data;

    }

    public function getAccount($id)
    {
        $data = $this->accountRepo->getAccount($id);
        return $data;
    }
    
    public function store($request)
    {
        $this->accountRepo->createAccount($request->all());
        
        return true;
    }

    public function update($request,$id)
    {
        $this->accountRepo->update($id,$request->all());
        return true;
    }

    public function delete($id)
    {
        $this->accountRepo->destroy($this->accountRepo->getById($id));
        return true;
    }


    //using repo
    public function getAllAccount()
    {
        
        $data = $this->accountRepo->getAllAccount();
        return $data;
    }



    public function createAccount($request)
    {
        $data = $this->accountRepo->createAccount($request->all());
    }

    public function updateAccount($request,$id)
    {
        $data = $this->accountRepo->updateAccount($request,$id);
        return $data;
    }

    public function deleteAccount($id)
    {
        $data = $this->accountRepo->deleteAccount($id);
        return $data;
    }
}
