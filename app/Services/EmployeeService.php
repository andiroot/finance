<?php
namespace App\Services;

use App\Repositories\Employee\EmployeeRepository;
use App\Models\Payroll\Employee;


class EmployeeService
{
    protected $employeeRepo;

    public function __construct(EmployeeRepository $employeeRepo)
    {
        $this->employeeRepo = $employeeRepo;
    }

    public function getAllEmployee()
    {
        $data = $this->employeeRepo->getAllEmployee();
        return $data;
    }

    public function getEmployee($id)
    {
        $data = $this->employeeRepo->getEmployee($id);
        return $data;
    }
    
    public function createEmployee($request)
    {
        $this->employeeRepo->store($request->all());
        return true;
    }

    public function updateEmployee($request,$id)
    {
        $this->employeeRepo->update($request-all(),$id);
        return true;
    }

    public function deleteEmployee($id)
    {
        $this->employeeRepo->destroy($this->employeeRepo->getById($id));
        return true;
    }
}