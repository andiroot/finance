<?php
namespace App\Services;

use App\Repositories\Loan\LoanRepository;
use App\Models\Payroll\Loan;


class LoanService
{
    protected $loanRepo;

    public function __construct(LoanRepository $loanRepo)
    {
        $this->loanRepo = $loanRepo;
    }

    public function getAllLoan()
    {
        $data = $this->loanRepo->getAllLoan();
        return $data;
    }

    public function getLoan($id)
    {
        $data = $this->loanRepo->getLoan($id);
        return $data;
    }

    public function createLoan($request)
    {
        $this->loanRepo->store($request);
        
        return true;
    }

    public function updateLoan($request,$id)
    {
        $this->loanRepo->update($request,$id);
        return true;
    }

    public function deleteLoan($id)
    {
        $this->loanRepo->destroy($this->loanRepo->getById($id));
        return true;
    }
}