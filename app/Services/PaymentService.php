<?php
namespace App\Services;

use App\Repositories\Payment\PaymentRepository;
use App\Models\Expense\Payment;


class PaymentService
{
    protected $paymentRepo;

    public function __construct(PaymentRepository $paymentRepo)
    {
        $this->paymentRepo = $paymentRepo;
    }
    public function getAllPayment()
    {
        $data = $this->paymentRepo->getAllPayment();
        return $data;
    }

    public function getPayment($id)
    {
        $data = $this->paymentRepo->getPayment($id);
        return $data;
    }
    
    public function createPayment($request)
    {
        $this->paymentRepo->store($request->all());
        return true;
    }

    public function updatePayment($request,$id)
    {
        $this->paymentRepo->update($request->all(),$id);
        return true;
    }

    public function deletePayment($id)
    {
        $this->paymentRepo->destroy($this->paymentRepo->getById($id));
        return true;
    }
}