<?php
namespace App\Services;

use App\Repositories\Reconciliation\ReconciliationRepository;
use App\Models\Banking\Reconciliation;


class ReconciliationService
{
    protected $reconciliationRepo;

    public function __construct(ReconciliationRepository $reconciliationRepo)
    {
        $this->reconciliationRepo = $reconciliationRepo;
    }

    public function getAllReconciliation()
    {
        $data = $this->reconciliationRepo->getAllReconciliation();
        return $data;
    }

    public function getReconciliation($id)
    {
        $data = $this->ReconciliationRepo->getReconciliation($id);
        return $data;
    }

    public function createReconciliation($request)
    {
        $this->reconciliationRepo->store($request);
        
        return true;
    }

    public function updateReconciliation($request,$id)
    {
        $this->reconciliationRepo->update($request,$id);
        return true;
    }

    public function deleteReconciliation($id)
    {
        $this->reconciliationRepo->destroy($this->reconciliationRepo->getById($id));
        return true;
    }
}