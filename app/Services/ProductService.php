<?php
namespace App\Services;

use App\Repositories\Product\ProductRepository;
use App\Models\Inventory\Product;


class ProductService
{
    protected $productRepo;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepo= $productRepo;
    }

    public function getAllProduct()
    {
        $data = $this->productRepo->getAllProduct();
        return $data;
    }

    public function getProduct($id)
    {
        $data = $this->productRepo->getProduct($id);
        return $data;
    }
    
    public function createProduct($request)
    {
        $this->productRepo->store($request->all());
        return true;
    }

    public function updateProduct($request,$id)
    {
        $this->productRepo->update($request->all(),$id);
        return true;
    }

    public function deleteProduct($id)
    {
        $this->productRepo->destroy($this->productRepo->getById($id));
        return true;
    }
}
