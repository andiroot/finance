<?php
namespace App\Services;

use App\Repositories\Inventory\WarehouseRepository;
use App\Models\Inventory\Warhouse;


class WarehouseService
{
    protected $warehouseRepo;

    public function __construct(WarehouseRepository $warehouseRepo)
    {
        $this->warehouseRepo= $warehouseRepo;
    }

    //using trait
    public function getAll()
    {
        $data = $this->warehouseRepo->all();
        return $data;

    }

    public function getWarehouse($id)
    {
        $data = $this->warehouseRepo->getWarehouse($id);
        return $data;
    }
    
    public function store($request)
    {
        $this->warehouseRepo->createWarehouse($request->all());
        
        return true;
    }

    public function update($request,$id)
    {
        $this->warehouseRepo->update($id,$request->all());
        return true;
    }

    public function delete($id)
    {
        $this->warehouseRepo->destroy($this->warehouseRepo->getById($id));
        return true;
    }


    //using repo
    public function getAllWarehouse()
    {
        
        $data = $this->warehouseRepo->getWarehouse();
        return $data;
    }



    public function createWarehouse($request)
    {
        $data = $this->warehouseRepo->createWarehouse($request->all());
    }

    public function updateWarhouse($request,$id)
    {
        $data = $this->warehouseRepo->updateWarehouse($request,$id);
        return $data;
    }

    public function deleteWarehouse($id)
    {
        $data = $this->warehouseRepo->deleteWarehouse($id);
        return $data;
    }
}
