<?php
namespace App\Services;

use App\Repositories\City\CityRepository;
use App\Models\Common\City;


class CityService
{
    protected $citytRepo;

    public function __construct(CityRepository $cityRepo)
    {
        $this->cityRepo = $cityRepo;
    }

    public function getAllCity()
    {
        $data = $this->cityRepo->getAllCity();
        return $data;
    }

    public function getCity($id)
    {
        $data = $this->cityRepo->getById($id);
        return $data;
    }
    
    public function createCity($request)
    {
        $data = $this->cityRepo->createCity($request);
        return true;
    }

    public function updateCity($request,$id)
    {
        $this->cityRepo->update($request->all(),$id);
        return true;
    }

    public function deleteCity($id)
    {
        $this->cityRepo->destroy($this->cityRepo->getById($id));
        return true;
    }
}