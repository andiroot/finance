<?php
namespace App\Services;

use App\Repositories\Contact\ContactRepository;
use App\Models\Common\Contact;


class ContactService
{
    protected $contactRepo;

    public function __construct(ContactRepository $contactRepo)
    {
        $this->contactRepo = $contactRepo;
    }
    public function getAllContact()
    {
        $data = $this->contactRepo->getAllContact();
        return $data;
    }

    public function getContact($id)
    {
        $data = $this->contactRepo->getById($id);
        return $data;
    }

    public function createContact($requst)
    {
        $this->contactRepo->store($requst);
        return true;
    }

    public function updateContact($requst,$id)
    {
        $this->contactRepo->update($requst,$id);
        return true;
    }


    public function deleteContact($id)
    {
        $this->contactRepo()->destroy($this->contactRepo->getById($id));
        return true;
    }
}