<?php
namespace App\Services;

use App\Repositories\County\CountyRepository;
use App\Models\Common\County;


class CountyService
{
    protected $countyRepo;

    public function __construct(CountyRepository $countyRepo)
    {
        $this->countyRepo = $countyRepo;
    }

    public function getAllCounty()
    {
        $data = $this->countyRepo->getAllCounty();
        return $data;
    }

    public function getCounty($id)
    {
        $data = $this->countyRepo->getById($id);
        return $data;
    }
    
    public function createCounty($request)
    {
        $this->countyRepo->store($request->all());
        return true;
    }

    public function updateCounty($request,$id)
    {
        $this->countyRepo->update($request->all(),$id);
        return true;
    }

    public function deleteCounty($id)
    {
        $this->countyRepo->destroy($this->countyRepo->getById());
        return true;
    }

}