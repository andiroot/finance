<?php
namespace App\Services;

use App\Repositories\Invoice\InvoiceRepository;
use App\Models\Income\Invoice;


class InvoiceService
{
    protected $InvoiceRepo;

    public function __construct(InvoiceRepository $InvoiceRepo)
    {
        $this->InvoiceRepo = $InvoiceRepo;
    }

    public function getAllInvoice()
    {
        $data = $this->InvoiceRepo->getAllInvoice();
        return $data;
    }

    public function getInvoice($id)
    {
        $data = $this->InvoiceRepo->getInvoice($id);
        return $data;
    }

    public function createInvoice($request)
    {
        $this->InvoiceRepo->store($request);
        
        return true;
    }

    public function updateInvoice($request,$id)
    {
        $this->InvoiceRepo->update($request,$id);
        return true;
    }

    public function deleteInvoice($id)
    {
        $this->InvoiceRepo->destroy($this->customerRepo->getById($id));
        return true;
    }
}