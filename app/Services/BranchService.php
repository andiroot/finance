<?php
namespace App\Services;

use App\Repositories\Department\BranchRepository;
use App\Models\Payroll\Branch;


class BranchService
{
    protected $branchRepo;

    public function __construct(BranchRepository $branchRepo)
    {
        $this->branchRepo = $branchRepo;
    }

    public function getAllBranch()
    {
        $data = $this->branchRepo->getAllBranch();
        return $data;
    }

    public function getBranch($id)
    {
        return $this->branchRepo->getBranch($id);

    }

    public function createBranch($request)
    {
        $this->branchRepo->strore($request);
        return true;
    }

    public function updateRepo($request,$id)
    {
        $this->branchRepo->update($request,$id);
        return true;
    }

    public function deleteBranch($id)
    {
        $this->branchRepo->destroy($this->branchRepo->getById());
        return true;
    }

}
