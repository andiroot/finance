<?php
namespace App\Services;

use App\Repositories\Order\OrderRepository;
use App\Models\Income\Order;


class OrderService
{
    protected $orderRepo;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepo = $orderRepo;
    }
    public function getAllOrder()
    {
        $data = $this->orderRepo->getAllOrder();
        return $data;
    }

    public function getOrder($id)
    {
        $data = $this->orderRepo->getOrder($id);
        return $data;
    }
    
    public function createOrder($request)
    {
        $this->orderRepo->store($request->all());
        return true;
    }

    public function updateOrder($request,$id)
    {
        $this->orderRepo->update($request->all(),$id);
        return true;
    }

    public function deleteOrder($id)
    {
        $this->orderRepo->destroy($this->orderRepo->getById($id));
        return true;
    }

}