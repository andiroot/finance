<?php
namespace App\Services;

use App\Repositories\Tax\TaxRepository;
use App\Models\Common\Tax;


class CustomerService
{
    protected $taxRepo;

    public function __construct(TaxRepository $customerRepo)
    {
        $this->taxRepo = $taxRepo;
    }

    public function getAllTax()
    {
        $data = $this->taxRepo->getAllTax();
        return $data;
    }

    public function getTax($id)
    {
        $data = $this->taxRepo->getTax($id);
        return $data;
    }

    public function createTax($request)
    {
        $this->taxRepo->store($request);
        
        return true;
    }

    public function updateTax($request,$id)
    {
        $this->taxRepo->update($request,$id);
        return true;
    }

    public function deleteTax($id)
    {
        $this->taxRepo->destroy($this->taxRepo->getById($id));
        return true;
    }
}