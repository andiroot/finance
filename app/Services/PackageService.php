<?php
namespace App\Services;

use App\Repositories\Package\PackageRepository;
use App\Models\Inventory\Package;


class PackageService
{

    protected $packageRepo;

    public function __construct(PackageRepository $packageRepo)
    {
        $this->packageRepo = $packageRepo;
    }
    public function getAllPackage()
    {
        $data = $this->packageRepo->getAllPackage();
        return $data;
    }

    public function getPackage($id)
    {
        $data = $this->packageRepo->getPackage($id);
        return $data;
    }
    
    public function createPackage($request)
    {
        $this->packageRepo->store($request->all());
        return true;
    }

    public function updatePackage($request,$id)
    {
        $this->packageRepo->update($request->all(),$id);
        return true;

    }

    public function deletePackage($id)
    {
        $this->packageRepo->destroy($this->packageRepo->getById($id));
        return true;
    }
}
