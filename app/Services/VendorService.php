<?php
namespace App\Services;

use App\Repositories\Vendor\VendorRepository;
use App\Models\Expense\Vendor;


class VendorService
{
    protected $vendorRepo;

    public function __construct(VendorRepository $vendorRepo)
    {
        $this->vendorRepo = $vendorRepo;
    }

    public function getAllVendor()
    {
        $data = $this->vendorRepo->getAllVendor();
        return $data;
    }

    public function getVendor($id)
    {
        $data = $this->vendorRepo->getVendor($id);
        return $data;
    }

    public function createVendor($request)
    {
        $this->vendorRepo->createVendor($request);
        return true;
    }

    public function updateVendor($request,$id)
    {
        $this->vendorRepo->update($request->all(),$id);
        return true;
    }

    public function deleteVendor($id)
    {
        $this->vendorRepo->destroy($this->vendorRepo->getById($id));
        return true;
    }
}