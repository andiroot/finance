<?php
namespace App\Services;

use App\Repositories\Department\DepartmentRepository;
use App\Models\Payroll\Department;


class DepartmentService
{
    protected $departmentRepo;

    public function __construct(DepartmentRepository $departmentRepo)
    {
        $this->departmentRepo = $departmentRepo;
    }

    public function getAllDepartment()
    {
        $data = $this->departmentRepo->getAllDepartment();
        return $data;
    }

    public function getDepartment($id)
    {
        $data = $this->departmentRepo->getDepartment($id);
        return $data;
    }

    public function createDepartment($request)
    {
        $this->departmentRepo->store($request->all());
        return true;
    }

    public function updateDepartment($request,$id)
    {
        $this->departmentRepo->update($request->all(),$id);
        return true;
    }

    public function deleteDepartment($id)
    {
        $this->departmentRepo->destroy($this->departmentRepo->getById($id));
        return true;
    }
}