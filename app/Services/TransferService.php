<?php
namespace App\Services;

use App\Repositories\Transfer\TransferRepository;
use App\Models\Banking\Transfer;

class TransferService
{
    protected $transferRepo;

    public function __construct(TransferRepository $transferRepo)
    {
        $this->transferRepo = $transferRepo;
    }

    public function getAllService()
    {
        $data = $this->transferRepo->getAllTransfer();
        return $data;
    }

    public function getTransfer($id)
    {
        return $this->transferRepo->getById($id);
    }
    
    public function createTransfer($request)
    {
        $this->transferRepo->store($request->all());
        return true;
    }

    public function updateTransfer($request,$id)
    {
        $this->transferRepo->update($request->all(),$id);
        return true;
    }

    public function deleteTransfer($id)
    {
        $this->transferRepo->destroy($this->transferRepo->getById($id));
        return true;
    }
}