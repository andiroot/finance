<?php
namespace App\Services;

use App\Repositories\Uom\UomRepository;
use App\Models\Inventory\Uom;


class UomService
{
    protected $uomRepo;

    public function __construct(UomRepository $uomRepo)
    {
        $this->uomRepo = $uomRepo;
    }

    public function getAllUom()
    {
        $data = $this->uomRepo->getAllUom();
        return $data;
    }

    public function getUom($id)
    {
        $data = $this->uomRepo->getUom($id);
        return $data;
    }

    public function createUom($request)
    {
        $this->uomRepo->store($request->all());
        return true;
    }

    public function updateUom($request,$id)
    {
        $this->uomRepo->update($request->all(),$id);
        return true;
    }

    public function deleteUom($id)
    {
        $this->uomRepo->destroy($this->uomRepo->getById($id));
        return true;
    }
}
