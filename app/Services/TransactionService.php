<?php
namespace App\Services;

use App\Repositories\Transaction\TransactionRepository;
use App\Models\Banking\Transaction;

class TransactionService
{
    protected $transactionRepo;

    public function __construct(TransactionRepository $transactionRepo)
    {
        $this->transactionRepo = $transactionRepo;
    }

    public function getAllTransaction()
    {
        $data = $this->transactionRepo->getAll();
        return $data;
    }

    public function getTransaction($id)
    {
        $data = $this->transactionRepo->getById($id);
        return $data;
    }

    public function createTransaction()
    {
        $this->transactionRepo->store($request->all());
        return true;
    }

    public function updateTransaction()
    {
        $this->transactioRepo->update($id,$request->all());
        return true;
    }

    public function deleteTransaction()
    {
        $this->transactionRepo->destroy($this->transactionRepo->getById($id));
        return true;
    }
}
