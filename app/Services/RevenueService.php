<?php
namespace App\Services;

use App\Repositories\Revenue\RevenuesRepository;
use App\Models\Income\Revenue;


class RevenueService
{
    protected $revenueRepo;

    public function __construct(RevenuesRepository $revenueRepo)
    {
        $this->revenueRepo = $revenueRepo;
    }

    public function getAllRevenues()
    {
        $data = $this->revenueRepo->getAllRevenues();
        return $data;
    }

    public function getRevenues($id)
    {
        $data = $this->revenueRepo->getRevenues($id);
        return $data;
    }

    public function createRevenues($request)
    {
        $this->revenueRepo->store($request->all());
        return true;
    }

    public function updateRevenues($request,$id)
    {

    }

    public function deleteRevenues($id)
    {

    }
}
