<?php
namespace App\Services;

use App\Repositories\Shipping\ShippingRepository;
use App\Models\Common\Shipping;


class ShippingService
{
    protected $shippingRepo;

    public function __construct(ShippingRepository $shippingRepo)
    {
        $this->shippingRepo = $shippingRepo;
    }

    public function getAllShipping()
    {
        $data = $this->shippingRepo->getAllShipping();
        return $data;
    }

    public function getShipping($id)
    {
        $data = $this->shippingRepo->getShipping($id);
        return $data;
    }

    public function createShipping($request)
    {
        $this->shippingRepo->store($request);
        
        return true;
    }

    public function updateShipping($request,$id)
    {
        $this->shippingRepo->update($request,$id);
        return true;
    }

    public function deleteShipping($id)
    {
        $this->shippingRepo->destroy($this->shippingRepo->getById($id));
        return true;
    }
}