<?php
namespace App\Services;

use App\Repositories\Designation\DesignationRepository;
use App\Models\Payroll\Designation;


class DesignationService
{
    protected $designationRepo;

    public function __construct(DesignationRepository $designationRepo)
    {
        $this->designationRepo = $designationRepo;
    }
    public function getAllDesignation()
    {
        $data = $this->designationRepo->getAllDesignation();
        return $data;
    }

    public function getDesignation($id)
    {
        $data = $this->designationRepo->getDesignation($id);
        return $data;
    }
    
    public function createDesignation($request)
    {
        $this->designationRepo->store($request);
        return true;
    }

    public function updateDesignation($request,$id)
    {
        $this->designationRepo->update($resuest->all(),$id);
        return true;
    }

    public function deleteDesignation($id)
    {
        $this->designationRepo->destory($this->designationRepo->getById($id));
        return true;
    }
}
