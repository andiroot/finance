<?php
namespace App\Services;

use App\Repositories\Inventory\InventoryRepository;
use App\Models\Payroll\Inventory;


class InventoryService
{
    protected $inventoryRepo;

    public function __construct(InventoryRepository $inventoryRepo)
    {
        $this->inventoryRepo = $inventoryRepo;
    }

    public function getAllInventory()
    {
        $data = $this->inventoryRepo->getAllInventory();
        return $data;
    }

    public function getInventory($id)
    {
        $data = $this->inventoryRepo->getInventory($id);
        return $data;
    }
    
    public function createInventory($request)
    {
        $this->inventoryRepo->store($request->all());
        return true;
    }

    public function updateInventory($request,$id)
    {
        $this->inventoryRepo->update($request->all(),$id);
        return true;
    }

    public function deleteInventory($id)
    {
        $this->inventoryRepo->destroy($this->inventoryRepo->getById($id));
        return true;
    }
}
