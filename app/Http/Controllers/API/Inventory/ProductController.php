<?php

namespace App\Http\Controllers\API\Inventory;

use App\Events\ProductWasCreated;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Services\ProductService;

class ProductController extends Controller
{
  protected $productService;

  public function __construct(ProductService $productService)
  {
    $this->productService=$productService;
  }

  public function getAllProduct()
  {
    $data = $this->productService->getAllProduct();
         return response()->json($data);
  }

  public function getProduct($id)
  {
    $data= $this->productService->getProduct($id);
    if($data){
        return response([
            'status'=>'success',
            'message'=>$data
        ],200);
    }
    return response([
        'status'=>'error',
        'message'=>'product not found'
    ],404); 
  }
  
  public function createProduct(ProductStoreRequest $request)
  {
    $data= $this->productService->createProduct($request->all());
    if($data){
        return response([
            'status'=>'success',
            'message'=>'product added'
        ],201);
    }
    return response([
        'status'=>'success',
        'message'=>'failed to add product'
    ],400); 

    even(new ProductWasCreated($product));
  }

  public function updateProduct(ProductUpdateRequest $request,$id)
  {
    $data = $this->productService->updateProduct($request->all(),$id);
    if($data===true){
        return response([
            'status'=>'success',
            'message'=>'product Updated'
        ],200); 
    }
    return response([
        'status'=>'error',
        'message'=>'product failed to update'
    ],400);
  }

  public function deleteProduct($id)
  {
    $data=$this->productService->deleteProduct($id);

    if($data===1){
        return response([
        'status'=>'success',
        'message'=>'product deleted'
        ],204); 
    }
    return response([
        'status'=>'error',
        'message'=>'product failed to delete'
    ],404); 
  }
}
