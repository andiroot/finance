<?php

namespace App\Http\Controllers\API\Inventory;

use Illuminate\Http\Request;
use App\Http\Requests\UomStoreRequest;
use App\Http\Requests\UomUpdateRequest;
use App\Http\Controllers\API\Controller;
use App\Services\UomService;

class UomController extends Controller
{
    protected $uomService;

    public function __construct(UomService $uomService)
    {
        $this->uomService=$uomService;
    }

    public function getAllUom()
    {
        $data = $this->uomService->getAllUom();
        return response()->json($data);
    }

    public function getUom($id)
    {
        $data= $this->uomService->getUom($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Uom not found'
        ],404); 
    }
    
    public function createUom(UomStoreRequest $request)
    {
        $data= $this->uomService->createUom($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Unit added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Unit'
        ],400); 
    }

    public function updateUom(UomUpdateReuquest $request,$id)
    {
        $data = $this->uomService->updateUom($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Unit Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Unit failed to update'
        ],400);
    }

    public function deleteUom($id)
    {
        $data=$this->uomService->deleteUom($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Unit deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Unit failed to delete'
        ],404); 
    }
}
