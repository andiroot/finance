<?php

namespace App\Http\Controllers\API\Inventory;

use App\Models\Inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Inventory\InventoryUpdateRequest;
use App\Http\Requests\Inventory\InventoryStoreRequest;
use App\Services\InventoryService;

class InventoryController extends Controller
{
    protected $inventoryService;

    public function __construct(InventoryService $inventoryService)
    {
        $this->inventoryService=$inventoryService;
    }

    public function getAllInventory()
    {
        $data = $this->inventoryService->getAllInventory();
         return response()->json($data);
    }

    public function getInventory($id)
    {
        $data= $this->inventoryService->getInventory($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Inventory not found'
        ],404); 
    }
    
    public function createInventory($request)
    {
        $data= $this->inventoryService->createInventory($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Inventory added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Inventory'
        ],400); 
    }

    public function updateInventory($request,$id)
    {
        $data = $this->inventoryService->updateInventory($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Inventory Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Inventory failed to update'
        ],400);

    }

    public function deleteInventory($id)
    {
        $data=$this->inventoryService->deleteCustomer($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Inventory deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Inventory failed to delete'
        ],404); 
    }
}
