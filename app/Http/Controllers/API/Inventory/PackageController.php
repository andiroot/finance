<?php

namespace App\Http\Controllers\API\Inventory;
use App\Services\PackageService; 
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\PackageStoreRequest;
use App\Http\Requests\PackageUpdateRequest;

class PackageController extends Controller
{
    protected $packageService;

    public function __construct(PackageService $packageService)
    {
        $this->packageService=$packageService;
    }

    public function getAllPackage()
    {
        $data = $this->packageService->getAllPackage();
        return response()->json($data);
    }

    public function getPackage($id)
    {
        $data= $this->packageService->getPackage($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'package not found'
        ],404); 
    }
    
    public function createPackage($request)
    {
        $data= $this->packageService->createPackage($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'package added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add custopackagemer'
        ],400); 
    }

    public function updatePackage($request,$id)
    {
        $data = $this->packageService->updatePackage($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Package Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Package failed to update'
        ],400);
    }

    public function deletePackage($id)
    {
        $data=$this->packageService->deletePackage($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'package deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'package failed to delete'
        ],404); 
    }
}
