<?php

namespace App\Http\Controllers\API\Income;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Invoice\InvoiceStoreRequest;
use App\Http\Requests\Invoice\InvoiceUpdateRequest;
use App\Services\InvoiceService;

class InvoiceController extends Controller
{
    
     protected $invoiceService;
    
     public function __construct(InvoiceService $invoiceService)
     {

         $this->invoiceService=$invoiceService;
         
     }
        
     public function getAllInvoice()
     {
         $data = $this->invoiceService->getAllInvoice();
         return response()->json($data);
     }

     public function getInvoice($id)
     {
        $data= $this->invoiceService->getInvoice($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Invoice not found'
        ],404); 
     }

     public function createInvoice(InvoiceStoreRequest $request)
     {  
        $data= $this->invoiceService->createInvoice($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Invoice added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Invoice'
        ],400); 
     }

     public function updateInvoice(InvoiceUpdateRequest $request,$id)
     {
        $data = $this->invoiceService->updateInvoice($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Invoice Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Invoice failed to update'
        ],400);
     }

     public function deleteInvoice($id)
     {
        $data=$this->invoiceService->deleteInvoice($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Invoice deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Invoice failed to delete'
        ],404); 
     }
}
