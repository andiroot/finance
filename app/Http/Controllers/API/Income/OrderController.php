<?php

namespace  App\Http\Controllers\API\Income;

use App\Services\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\OrderStoreRequest;
use App\Http\Requests\OrderUpdateRequest;


class OrderController extends Controller
{
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService=$orderService;
    }

    public function getAllOrder()
    {
        $data = $this->orderService->getAllOrder();
         return response()->json($data);
    }

    public function getOrder($id)
    {
        $data= $this->orderService->getOrder($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Order not found'
        ],404); 
    }
    
    public function createOrder(OrderStoreRequest $request)
    {
        $data= $this->orderService->createOrder($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Order added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Order'
        ],400); 
    }

    public function updateOrder(OrderUpdateRequest $request,$id)
    {
        $data = $this->orderService->updateOrder($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Customer Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Order failed to update'
        ],400);
    }

    public function deleteOrder($id)
    {
        $data=$this->orderService->deleteOrder($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Order deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Order failed to delete'
        ],404); 
    }
}
