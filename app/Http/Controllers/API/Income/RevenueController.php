<?php

namespace  App\Http\Controllers\API\Income;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Services\RevenueService;
use App\Http\Requests\RevenueUpdateRequest;
use App\Http\Requests\RevenueStoreRequest;

class RevenueController extends Controller
{
    protected $revenueService;
    public function __construct(RevenueService $revenueService)
    {
        $this->revenueService = $revenueService;
    }

    public function getAllRevenues()
    {
        $data = $this->revenueService->getAllRevenues();
        return response()->json($data);
    }

    public function getRevenues($id)
    {
        $data= $this->revenueService->getRevenues($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Revenue not found'
        ],404); 
    }
    
    public function createRevenues(RevenueStoreRequest $request)
    {
        $data= $this->revenueService->createRevenues($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Revenue added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Revenue'
        ],400); 
     }
    

    public function updateRevenues(RevenueUpdateRequst $request,$id)
    {
        $data = $this->revenueService->updateRevenues($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Revenue Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Revenue failed to update'
        ],400);
    }

    public function deleteRevenues($id)
    {
        $data=$this->revenueService->deleteRevenues($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Revenue deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Revenue failed to delete'
        ],404); 
    }
}
