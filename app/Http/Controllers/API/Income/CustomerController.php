<?php

namespace App\Http\Controllers\API\Income;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Customer\CustomerStoreRequest;
use App\Http\Requests\Customer\CustomerUpdateRequest;
use App\Services\CustomerService;

class CustomerController extends Controller
{
    
     protected $customerService;
    
     public function __construct(CustomerService $customerService)
     {

         $this->customerService=$customerService;
         
     }
        
     public function getAllCustomer()
     {
         $data = $this->customerService->getAllCustomer();
         return response()->json($data);
     }

     public function getCustomer($id)
     {
        $data= $this->customerService->getCustomer($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'customer not found'
        ],404); 
     }

     public function createCustomer(CustomerStoreRequest $request)
     {  
        $data= $this->customerService->createCustomer($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'customer added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add customer'
        ],400); 
     }

     public function updateCustomer(CustomerUpdateRequest $request,$id)
     {
        $data = $this->customerService->updateCustomer($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Customer Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Customer failed to update'
        ],400);
     }

     public function deleteCustomer($id)
     {
        $data=$this->customerService->deleteCustomer($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'customer deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'customer failed to delete'
        ],404); 
     }
}
