<?php

namespace App\Http\Controllers\API\Banking;

use App\Services\AccountService;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Account\AccountStoreRequest;
use App\Http\Requests\Account\AccountUpdateRequest;

class AccountController extends Controller
{

    protected $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->middleware('auth:api');
         $this->accountService = $accountService;   
    }
    
    public function getAllAccount()
    {
        $data = $this->accountService->getAllAccount();
        return response()->json($data);
    }

    public function getAccount($id)
    {
        $data = $this->accountService->getAccount($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Account not found'
        ],404); 
    }

    public function createAccount(AccountStoreRequest $request)
    {
        //$data = $this->account->createAccount($request->all());
        $data = $this->accountService->store($request);
        if($data){
            return response([
                'status'=>'success',
                'message'=>'account added'
            ],201);
        }
        return response([
            'status'=>'error',
            'message'=>'failed to add account'
        ],400); 
        
    }

    public function updateAccount(AccountUpdateRequest $request,$id)
    {
        $data = $this->accountService->update($request,$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Account Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Account failed to update'
        ],400);
    }

    public function deleteAccount($id)
    {
        $data = $this->accountService->delete($id);
        if($data===true){
            return response([
            'status'=>'success',
            'message'=>'Account deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Account failed to delete'
        ],404); 
    }

}
