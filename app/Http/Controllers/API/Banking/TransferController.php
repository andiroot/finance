<?php

namespace App\Http\Controllers\API\Banking;

use App\Services\TransferService;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;

use APp\Http\Requests\TransferUpdateRequest;
use APp\Http\Requests\TransferStoreRequest;

class TransferController extends Controller
{
    protected $transferService;

    public function __construct(TransferService $transferService)
    {
        $this->transferService=$transferService;
    }

    public function getAll()
    {
        $data = $this->transferService->getAllService();
        return response()->json($data);
    }

    public function getTransfer($id)
    {
        $data= $this->transferService->getTransfer($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'transfer not found'
        ],404);
    }
    
    public function createTransfer(TransferStoreRequest $request)
    {
        $data= $this->transferService->createTransfer($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'transfer added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add transfer'
        ],400); 
    }

    public function updateTransfer(TransgerUpdateRequest $request,$id)
    {
        $data = $this->transferService->updateTransfer($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Transfer Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Transfer failed to update'
        ],400);
    }

    public function deleteTransfer($id)
    {
        $data=$this->transferService->deleteTransfer($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Transfer deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Transfer failed to delete'
        ],404); 
    }
}
