<?php

namespace App\Http\Controllers\API\Banking;

use App\Services\TransactionService;
use Illuminate\Http\Request;
use App\Http\Requests\TransactionUpdateRequest;
use App\Htttp\Requests\TransactionStoreRequest;
use App\Http\Controllers\API\Controller;
class TransactionController extends Controller
{
    protected $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    public function getAllTransaction()
    {
        $data = $this->transactionService->getAllTransaction();
        return response()->json($data);
    }

    public function getTransaction($id)
    {
        $data= $this->transaction->getTransaction($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'transaction not found'
        ],404); 
    }
    
    public function createTransaction(TransactionStoreRequest $request)
    {
        $data= $this->transaction->createTransaction($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'transaction added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add transaction'
        ],400); 
    }

    public function updateTransaction(TransactionUpdateRequest $request,$id)
    {
        $data = $this->transaction->updateTransaction($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Transaction Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Transaction failed to update'
        ],400);
    }

    public function deleteTransaction($id)
    {
        $data=$this->transaction->deleteTransaction($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Transaction deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Transaction failed to delete'
        ],404); 
    }

}
