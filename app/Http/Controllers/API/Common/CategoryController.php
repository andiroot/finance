<?php

namespace App\Http\Controllers\API\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Category\CategoryStoreRequest;
use App\Http\Requests\Category\CategoryUpdateRequest;
use App\Services\CategoryService;

class CategoryController extends Controller
{
  protected $categoryService;
  
  public function __construct(CategoryService $categoryService)
  {
    $this->categoryService=$categoryService;
  }
  public function getAllCategory()
  {
    $category = $this->categoryService->getAllCategory();
    return response()->json($category);
  }

  public function getCategory($id)
  {
    $category= $this->categoryService->getCategory($id);

    if($category){
        return response([
            'status'=>'success',
            'message'=>$category
        ],200);
    }
    return response([
        'status'=>'error',
        'message'=>'Category not found'
    ],404);
  }

  public function createCategory(CategoryStoreRequest $request)
  {
    $category = $this->categoryService->createCategory($request->all());

    if($category){
        return response([
            'status'=>'success',
            'message'=>'Category Updates'
        ],201);
    }
    return response([
        'status'=>'error',
        'message'=>'Category failed to update'
    ],400);
  }

  public function updateCategory(CategoryUpdateRequest $request,$id)
  {
    $update = $this->categoryService->updateCategory($request->all(),$id);
    if($update===true){
        return response([
            'message'=>'success',
            'message'=>'Category Updated'
        ],200);
    }

    return response([
        'status'=>'error',
        'message'=>'Category Failed to update'
    ],400);
  }

  public function deleteCategory()
  {
    $delete = $this->categoryService->deleteCategory($id);

    if($delete===1){
        return response([
            'status'=>'success',
            'message'=>'category deleted'
        ],204);
    }
    return response([
        'status'=>'error',
        'message'=>'category failed to delete'
    ],404);
  }
} 
