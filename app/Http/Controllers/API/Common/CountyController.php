<?php

namespace App\Http\Controllers\API\Common;

use App\Services\CountyService;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\County\CountyStoreRequest;
use App\Http\Requests\County\CountyUpdateRequest;


class CountyController extends Controller
{
    protected $countyService;

    public function __construct(CountyService $countyService)
    {
        $this->countyService = $countyService;
    }
    
    public function getAllCounty()
    {
        $county = $this->countyService->getAllCounty();
        return response()->json($county);
    }

    public function getCounty($id)
    {   
        $county = $this->countyService->getCounty($id);
        if($county){
            return response([
                'status'=>'success',
                'message'=>$county
            ],200);
        }

        return response([
            'status'=>'success',
            'message'=>'County Not Found'
        ],404);
    }
    
    public function createCounty(CountyStoreRequest $request)
    {
        $county = $this->countyService->createCounty($request->all());
        
        if($county){
            return response([
                'status'=>'success',
                'message'=>'County Addes'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add county'
        ],400);
    }

    public function updateCounty($request,$id)
    {
        $update = $this->countyService->updateCounty($request->all(),$id);
        if($update===true){
            return response([
                'status'=>'success',
                'message'=>'Customer Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Customer failed to update'
        ],400);
    }

    public function deleteCounty($id)
    {
        $delete=$this->countyService->deleteCounty($id);

        if($delete===1){
            return response([
            'status'=>'success',
            'message'=>'county deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'county failed to delete'
        ],404); 
    }
}
