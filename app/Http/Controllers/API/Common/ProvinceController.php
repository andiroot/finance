<?php

namespace App\Http\Controllers\API\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Services\ProvinceService;
use App\Http\Requests\Province\ProvinceStoreRequest;
use App\Http\Requests\Province\ProvinceUpdateRequest;

class ProvinceController extends Controller
{
    protected $provinceService;
    public function __construct(ProvinceService $provinceService)
    {
        $this->provinceService=$provinceService;
    }

    public function getAllProvince()
    {
        $data = $this->provinceService->getAllProvince();
        return response()->json($data);
    }

    public function getProvince($id)
    {
        $data = $this->provinceService->getProvince($id);
        
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Province not found'
        ],404);
    }
    
    public function createProvince(ProvinceStoreRequest $request)
    {
        $data= $this->provinceService->createProvince($request->all());
        
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Province added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Province'
        ],400); 
    }

    public function updateProvince(ProvinceUpdateRequest $request,$id)
    {
        $data = $this->provinceService->updateProvince($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Province Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Province failed to update'
        ],400);
    }

    public function deleteProvince($id)
    {
        $data=$this->provinceService->deleteProvince($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Province deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Province failed to delete'
        ],404); 
    }
}
