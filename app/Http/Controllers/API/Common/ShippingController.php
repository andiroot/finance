<?php

namespace App\Http\Controllers\API\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Shipping\ShippingStoreRequest;
use App\Http\Requests\Shipping\ShippingUpdateRequest;
use App\Services\ShippingService;

class ShippingController extends Controller
{
    
     protected $ShippingService;
    
     public function __construct(ShippingService $ShippingService)
     {

         $this->ShippingService=$ShippingService;
         
     }
        
     public function getAllShipping()
     {
         $data = $this->ShippingService->getAllShipping();
         return response()->json($data);
     }

     public function getShipping($id)
     {
        $data= $this->ShippingService->getShipping($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Shipping not found'
        ],404); 
     }

     public function createShipping(ShippingStoreRequest $request)
     {  
        $data= $this->ShippingService->createShipping($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Shipping added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Shipping'
        ],400); 
     }

     public function updateShipping(ShippingUpdateRequest $request,$id)
     {
        $data = $this->ShippingService->updateShipping($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Shipping Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Shipping failed to update'
        ],400);
     }

     public function deleteShipping($id)
     {
        $data=$this->ShippingService->deleteShipping($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Shipping deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Shipping failed to delete'
        ],404); 
     }
}
