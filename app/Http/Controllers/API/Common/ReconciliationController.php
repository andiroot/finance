<?php

namespace App\Http\Controllers\API\Banking;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Reconciliation\ReconciliationStoreRequest;
use App\Http\Requests\Reconciliation\ReconciliationUpdateRequest;
use App\Services\CustomerService;

class ReconciliationController extends Controller
{
    
     protected $reconciliationService;
    
     public function __construct(ReconciliationService $customerService)
     {

         $this->reconciliationService=$reconciliationService;
         
     }
        
     public function getAllReconciliation()
     {
         $data = $this->reconciliationService->getAllReconciliation();
         return response()->json($data);
     }

     public function getReconciliation($id)
     {
        $data= $this->reconciliationService->getReconciliation($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Reconciliation not found'
        ],404); 
     }

     public function createReconciliation(ReconciliationStoreRequest $request)
     {  
        $data= $this->ReconciliationService->createReconciliation($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Reconciliation added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Reconciliation'
        ],400); 
     }

     public function updateReconciliation(ReconciliationUpdateRequest $request,$id)
     {
        $data = $this->reconciliationService->updateReconciliation($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Reconciliation Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Reconciliation failed to update'
        ],400);
     }

     public function deleteReconciliation($id)
     {
        $data=$this->ReconciliationService->deleteReconciliation($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Reconciliation deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Reconciliation failed to delete'
        ],404); 
     }
}
