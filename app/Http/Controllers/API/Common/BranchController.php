<?php

namespace App\Http\Controllers\API\Payroll;

use App\Services\BranchService;
use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Payroll\BranchUpdateRequest;
use App\Http\Payroll\BranchStoreRequst;

class BranchController extends Controller
{
    protected $branchService;
    
    public function __construct(BranchService $departmentService)
    {
        $this->branchService=$branchService;
    }

   
    public function getAllBranch()
    {
        $data = $this->branchService->getAllBranch();
        return response()->json($data);
    }

    public function getBranch($id)
    {
        $data= $this->branchService->getBranch($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Department not found'
        ],404); 
    }
    
    public function createBranch(BranchStoreRequest $request)
    {
        $data= $this->branchService->createBranch($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Branch added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add branch'
        ],400); 
    }

    public function updateBranch(BranchUpdateRequest $request,$id)
    {
        $data = $this->branchService->updateBranch($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Branch Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Department failed to update'
        ],400);
    }

    public function deleteBranch($id)
    {
        $data=$this->branchService->deleteBranch($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Branch deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Branch failed to delete'
        ],404); 
    }
}