<?php
namespace App\Http\Controllers\API\Common;

use App\Services\ContactService;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Contact\ContactStoreRequest;
use App\Http\Requests\Contact\ContactUpdateRequest;

class ContactController extends Controller
{
    
    protected $contactService;    
    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    public function getAllContact()
    {
        $contact = $this->contactService->getAllContact();
        return response()->json($contact);

    }

    public function getContact($id)
    {
        $data= $this->contactService->getContact($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Contact not found'
        ],404); 
    }

    public function createContact(ContactStoreRequest $request)
    {
        $data= $this->contactService->createContact($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Contact added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Contact'
        ],400); 

    }

    public function updateContact($requst,$id)
    {
        $data = $this->contactService->updateContact($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Contact Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Contact failed to update'
        ],400);

    }

    public function deleteContact($id)
    {
        $data=$this->contactService->deleteContact($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'contact deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'contact failed to delete'
        ],404); 

    }
}
