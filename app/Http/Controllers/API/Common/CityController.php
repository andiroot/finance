<?php

namespace App\Http\Controllers\API\Common;

use App\Http\Controllers\API\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\City\CityStoreRequest;
use App\Http\Requests\City\CityUpdateRequest;
use App\Services\CityService;

class CityController extends Controller
{
    protected $cityService;

    public function __construct(CityService $cityService)
    {
        $this->cityService = $cityService;
    }

    public function getAllCity()
    {
        $city = $this->cityService->getAllCity();
        return response()->json($city);
    }

    public function getCity($id)
    {
        $city = $this->cityService->getCity($id);

        if($city){
            return response([
                'status'=>'success',
                'message'=>$city
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'City Not Found'
        ],404);
    }
    
    public function createCity(CityStoreRequest $request)
    {
        $city = $this->cityService->createCity($request);

        if($city){
            return response([
                'status'=>'success',
                'message'=>'city added'
            ],201);
        }

        return response([
            'status'=>'Fail',
            'message'=>'failed to add city'
        ],400);
    }

    public function updateCity(CityUpdateRequest $request,$id)
    {
        $update = $this->cityService->updateCity($request->all());
        if($update===true){
            return response([
                'status'=>'success',
                'message'=>'city updated'
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'city failed to update'
        ],400);
    }

    public function deleteCity($id)
    {
        $delete = $this->cityService->delete($id);

        if($delete===1){
            return response([
                'status'=>'success',
                'message'=>'city deleted'
            ],204);
        }
        return response([
            'success'=>'error',
            'message'=>'city failed to delete'
        ],404);
    }


}
