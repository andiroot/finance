<?php

namespace App\Http\Controllers\API\Common;

use App\Deal;
use Illuminate\Http\Request;



class DealController extends Controller
{
    
  /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /*
       return Deal::join('users','users.id')
                ->select('deals.id','deals.reportdate','deals.name','deals.lead','deals.closing','deals.payment_method','deals.type','deals.notes','users.id')
                ->where('user.id')
                ->latest()
                ->paginate(10);
       */
        /*

                DB::connection('conn_name')->table('table1 as t1')
                     ->select("t1.col_name", "t2.col_name","t3.col_name" )             
                     ->join("table2 AS t2", "t1.id", "=", "t2.id")
                     ->join("table3 AS t3", "t3.id", "=", "t2.id")
                     ->where("col_name", "=",”some_value”)
                     ->get();
        */
        /*
            return Order::join('products','products.id','orders.product_id')
                ->join('users','users.id','orders.users_id')
                ->select('orders.id as id','users.name as cs_name','users.cs_phone','orders.cust_name as cust_name',
                        'orders.cust_phone as cust_phone','orders.cust_address cust_address','product.name as product',
                        'products.price as price','orders.qty','orders.payment_method as payment_method','orders.courier',
                        'orders.receipt_number as receipt_number','orders.postal_fee as postal_fee','orders.sub_total as ',
                        'orders.notes as notes','orders.status_delivery as status_delivery ')
                ->paginate(10);

        */
        /*
        return Deal::join('users','users.id','deals.user_id')
                
                        ->select('deals.id as id',
                                'deals.reportdate as reportdate ',
                                'deals.name as product ',
                                'deals.lead as leads ',
                                'deals.closing as closings ',
                                'deals.payment_method as payment_method',
                                'deals.type as type',
                                'deals.notes as notes'
                                )
                                ->where('deals.user_id','=',$user_id)
                                ->latest()
                                ->paginate(10);
        */
        if (\Gate::allows('isUser') || \Gate::allows('isAdmin')) {
            // The current user can edit settings
                $user_id= Auth::user()->id;

             return Deal::where('deals.user_id','=',$user_id)
                                ->latest()
                                ->paginate(10);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'reportdate'=>'required',
            'name'  => 'required',
            'lead' => 'required',
            'closing'=>'required',
            'payment_method'=>'required',
            'type'=>'required',
            'notes'=>'required',
        ]);
        
        return Deal::create([
            'reportdate'      =>   $request['reportdate'],
            'name'     =>   $request['name'],
            'lead'      =>   $request['lead'],
            'closing'      =>   $request['closing'],
            'payment_method'      =>   $request['payment_method'],
            'type'  =>  $request['type'],
            'notes'=> $request['notes'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function show(Deal $deal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function edit(Deal $deal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isUser');
        $deal =Deal::findOrFail($id);

        $this->validate($request,[
            'reportdate'=>'required',
            'name'  => 'required',
            'lead' => 'required',
            'closing'=>'required',
            'payment_method'=>'required',
            'type'=>'required',
            'notes'=>'required',
        ]);
        
        $deal->update($request->all());
        return ['message','Update  the user info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isUser');
        $deal = Deal::findOrFail($id);
        //delete the user
        $deal->delete();
        return ['message'=>'User Deleted'];
    }

    public function search()
    {
        if($search = \Request::get('q')){
            $deals = Deal::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%")
                        ->orWhere('name','LIKE',"%$search%");
            })->paginate(20);
        }else{
            //kalau ga ada query di search jalanin ini
            $deals = Deal::latest()->paginate();
        }
        return $deals;
    }
}
