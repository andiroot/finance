<?php

namespace App\Http\Controllers\API\Inventory;

use App\Services\WarehouseService;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Inventory\WarhouseStoreRequest;
use App\Http\Requests\Inventory\WarhouseUpdateRequest;

class WarehouseController extends Controller
{

    protected $warehouseService;

    public function __construct(WarehouseService $warehouseService)
    {
        $this->middleware('auth:api');
         $this->warehouseService = $warehouseService;   
    }
    
    public function getAllWarehouse()
    {
        $data = $this->warehouseService->getAllWarehouse();
        return response()->json($data);
    }

    public function getWarehouse($id)
    {
        $data = $this->warehouseService->getWarehouse($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Not found'
        ],404); 
    }

    public function createWarehouse(WarehouseStoreRequest $request)
    {
        //$data = $this->account->createAccount($request->all());
        $data = $this->warehouseService->store($request);
        if($data){
            return response([
                'status'=>'success',
                'message'=>'account added'
            ],201);
        }
        return response([
            'status'=>'error',
            'message'=>'failed to add account'
        ],400); 
        
    }

    public function updateWarehouse(WarehouseUpdateRequest $request,$id)
    {
        $data = $this->warehouseService->update($request,$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>' Warehouse failed to update'
        ],400);
    }

    public function deleteWarehouse($id)
    {
        $data = $this->warehouseService->delete($id);
        if($data===true){
            return response([
            'status'=>'success',
            'message'=>'Warehouse deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Warehouse failed to delete'
        ],404); 
    }

}
