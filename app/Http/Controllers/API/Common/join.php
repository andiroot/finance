return Order::join('products','products.id','orders.product_id')
                ->join('users','users.id','orders.users_id')
                ->select('orders.id as id','users.name as cs_name','users.cs_phone','orders.cust_name as cust_name',
                        'orders.cust_phone as cust_phone','orders.cust_address cust_address','product.name as product',
                        'products.price as price','orders.qty','orders.payment_method as payment_method','orders.courier',
                        'orders.receipt_number as receipt_number','orders.postal_fee as postal_fee','orders.sub_total as ',
                        'orders.notes as notes','orders.status_delivery as status_delivery ')
                ->paginate(10);