<?php

namespace App\Http\Controllers\API\Common;

use App\Models\taxes;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Tax\TaxStoreRequest;
use App\Http\Requests\Tax\TaxUpdateRequest;
use App\Services\TaxService;

class TaxesController extends Controller
{

    protected $taxService;
    
    public function __construct(TaxService $taxService)
    {
        $this->taxService=$taxService;    
    }

    public function getAllTax()
    {
        $data = $this->taxService()->getAllTax();
        return response()->json($data);
    }
    public function getTax($id)
    {
       $data= $this->taxService->getTax($id);
       if($data){
           return response([
               'status'=>'success',
               'message'=>$data
           ],200);
       }
       return response([
           'status'=>'error',
           'message'=>'tax not found'
       ],404); 
    }

    public function createTax(TaxStoreRequest $request)
    {  
       $data= $this->taxService->createTax($request->all());
       if($data){
           return response([
               'status'=>'success',
               'message'=>'tax added'
           ],201);
       }
       return response([
           'status'=>'success',
           'message'=>'failed to add customer'
       ],400); 
    }

    public function updateTax(TaxUpdateRequest $request,$id)
    {
       $data = $this->taxService->updateTax($request->all(),$id);
       if($data===true){
           return response([
               'status'=>'success',
               'message'=>'Tax Updated'
           ],200); 
       }
       return response([
           'status'=>'error',
           'message'=>'Tax failed to update'
       ],400);
    }

    public function deleteTax($id)
    {
       $data=$this->taxService->deleteTax($id);

       if($data===1){
           return response([
           'status'=>'success',
           'message'=>'tax deleted'
           ],204); 
       }
       return response([
           'status'=>'error',
           'message'=>'tax failed to delete'
       ],404); 
    }
}
