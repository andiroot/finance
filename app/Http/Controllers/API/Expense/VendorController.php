<?php

namespace App\Http\Controllers\API\Expense;


use Illuminate\Http\Request;
use App\Http\Requests\Vendor\VendorStoreRequest;
use App\Http\Requests\Vendor\VendorUpdateRequest;
use App\Http\Controllers\API\Controller;
use App\Services\VendorService;

class VendorController extends Controller
{
    protected $vendorService;

    public function __construct(VendorService $vendorService)
    {
        $this->vendorService=$vendorService;
    }

    public function getAllVendor()
    {
        $data = $this->vendorService->getAllVendor();
         return response()->json($data);
    }

    public function getVendor($id)
    {
        $data= $this->vendorService->getVendor($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Vendor not found'
        ],404); 
    }
    
    public function createVendor(VendorStoreRequest $request)
    {
        $data= $this->vendorService->createVendor($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Vendor added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Vendor'
        ],400); 
    }

    public function updateVendor($request,$id)
    {
        $data = $this->vendorService->updateVendor($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Vendor Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Vendor failed to update'
        ],400);
    }

    public function deleteVendor($id)
    {
        $data=$this->vendorService->deleteVendor($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Vendor deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Vendor failed to delete'
        ],404); 
    }
}
