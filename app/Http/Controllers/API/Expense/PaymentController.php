<?php

namespace App\Http\Controllers\API\Expense;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\PaymentStoreRequest;
use App\Http\Requests\PaymentUpdateRequest;
use App\Services\PaymentService;

class PaymentController extends Controller
{
    protected $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService=$paymentService;
    }

    public function getAllPayment()
    {
        $data = $this->paymentService->getAllPayment();
        return response()->json($data);
    }

    public function getPayment($id)
    {
        $data= $this->paymentService->getPayment($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Payment not found'
        ],404); 
    }
    
    public function createPayment(PaymenyStoreRequest $request)
    {
        $data= $this->paymentService->createPayment($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Payment added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Payment'
        ],400); 
    }

    public function updatePayment(PaymentUpdateRequest $request,$id)
    {
        $data = $this->paymentService->updatePayment($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Payment Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Payment failed to update'
        ],400);
    }

    public function deletePayment($id)
    {
        $data=$this->paymentService->deletePayment($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Payment deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Payment failed to delete'
        ],404); 
    }

}
