<?php

namespace App\Http\Controllers\API\Payroll;

use App\Services\DesignationService;
use App\Http\Controllers\API\Controller;
use Illuminate\Http\Request;
use App\Models\Payroll\Designation;
use App\Http\Requests\Designation\DesignationStoreRequest;
use App\Http\Requests\Designation\DesignationUpdateRequest;

class DesignationController extends Controller
{
    protected $designationService;
    public function __construct(DesignationService $designationService)
    {
        $this->designationService= $designationService;
    }

    public function getAllDesignation()
    {
        $data = $this->designationService->getAllDesignation();
         return response()->json($data);
    }

    public function getDesignation($id)
    {   
        $data= $this->designationService->getDesignation($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Designation not found'
        ],404); 
    }
    
    public function createDesignation(DesignationStoreRequest $request)
    {
        $data= $this->designationService->createDesignation($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Designation added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'Failed to add Designation'
        ],400); 
    }

    public function updateDesignation(DesignationUpdateRequest $request,$id)
    {
        $data = $this->designationService->updateDesignation($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Designation Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Designation failed to update'
        ],400);
    }

    public function deleteDesignation($id)
    {
        $data=$this->designationService->deleteDesignation($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Designation deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Designation failed to delete'
        ],404); 
    }
}
