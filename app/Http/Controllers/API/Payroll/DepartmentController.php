<?php

namespace App\Http\Controllers\API\Payroll;

use App\Services\DepartmentService;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Payroll\DepartmentUpdateRequest;
use App\Http\Payroll\DepartmentStoreRequst;

class DepartmentController extends Controller
{
    protected $departmentService;
    
    public function __construct(DepartmentService $departmentService)
    {
        $this->departmentService=$departmentService;
    }

    public function getAllDepartment()
    {
        $data = $this->departmentService->getAllDepartment();
        return response()->json($data);
    }

    public function getDepartment($id)
    {
        $data= $this->departmentService->getDepartment($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Department not found'
        ],404); 
    }
    
    public function createDepartment(DepartmentStoreRequest $request)
    {
        $data= $this->departmentService->createDepartment($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Department added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add department'
        ],400); 
    }

    public function updateDepartment(DepartmentUpdateRequest $request,$id)
    {
        $data = $this->departmentService->updateDepartment($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Department Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Department failed to update'
        ],400);
    }

    public function deleteDepartment($id)
    {
        $data=$this->departmentService->deleteDepartment($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Department deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Department failed to delete'
        ],404); 
    }
}
