<?php

namespace App\Http\Controllers\API\Income;

use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;
use App\Http\Requests\Loan\LoanStoreRequest;
use App\Http\Requests\Loan\LoanUpdateRequest;
use App\Services\LoanService;

class LoanController extends Controller
{
    
     protected $loanService;
    
     public function __construct(LoanService $loanService)
     {

         $this->loanService=$loanService;
         
     }
        
     public function getAllLoan()
     {
         $data = $this->loanService->getAllLoan();
         return response()->json($data);
     }

     public function getLoan($id)
     {
        $data= $this->loanService->getLoan($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Loan not found'
        ],404); 
     }

     public function createLoan(LoanStoreRequest $request)
     {  
        $data= $this->loanService->createLoan($request);
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Loan added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Loan'
        ],400); 
     }

     public function updateLoan(LoanUpdateRequest $request,$id)
     {
        $data = $this->loanService->updateLoan($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Loan Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Loan failed to update'
        ],400);
     }

     public function deleteLoan($id)
     {
        $data=$this->loanService->deleteLoan($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Loan deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Loan failed to delete'
        ],404); 
     }
}
