<?php

namespace App\Http\Controllers\API\Payroll;

use App\Services\EmployeeService;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Controller;

class EmployeeController extends Controller
{
    protected $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService=$employeeService;

    }

    public function getAllEmployee()
    {
        $data = $this->employeeService->getAllEmployee();
         return response()->json($data);
    }

    public function getEmployee($id)
    {
        $data= $this->employeeService->getEmployee($id);
        if($data){
            return response([
                'status'=>'success',
                'message'=>$data
            ],200);
        }
        return response([
            'status'=>'error',
            'message'=>'Employee not found'
        ],404); 
    }
    
    public function createEmployee($request)
    {
        $data= $this->employeeService->createEmployee($request->all());
        if($data){
            return response([
                'status'=>'success',
                'message'=>'Employee added'
            ],201);
        }
        return response([
            'status'=>'success',
            'message'=>'failed to add Employee'
        ],400); 
    }

    public function updateEmployee($request,$id)
    {
        $data = $this->employeeService->updateEmployee($request->all(),$id);
        if($data===true){
            return response([
                'status'=>'success',
                'message'=>'Employee Updated'
            ],200); 
        }
        return response([
            'status'=>'error',
            'message'=>'Employee failed to update'
        ],400);
    }

    public function deleteEmployee($id)
    {
        $data=$this->employeeService->deleteCustomer($id);

        if($data===1){
            return response([
            'status'=>'success',
            'message'=>'Employee deleted'
            ],204); 
        }
        return response([
            'status'=>'error',
            'message'=>'Employee failed to delete'
        ],404); 
    }
}
