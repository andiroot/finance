<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor_id'=>'required',
            'category_id'=>'required',
            'uom_id'=>'required',
            'tax_id'=>'required',
            'name'=>'required',
            'sku'=>'required',
            'description'=>'required',
            'sale_price'=>'required',
            'purchase_price'=>'required',
            'capital_price'=>'required',
            'qty'=>'required',
            'weight'=>'required',
            'price_closing'=>'required',
            'enabled'=>'required',
        ];
    }
}
