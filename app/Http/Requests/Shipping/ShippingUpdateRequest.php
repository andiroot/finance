<?php

namespace App\Http\Requests\Shipping;

use Illuminate\Foundation\Http\FormRequest;

class ShippingUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id'=>'required',
            'order_id'=>'required',
            'courier_id'=>'required',
            'type'=>'required',
            'shipping_number'=>'required',
            'manifest_city'=>'required',
            'manifest_description'=>'required',
            'description'=>'required',
            'notes'=>'required',
            'status'=>'required',
        ];
    }
}
