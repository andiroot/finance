<?php

namespace App\Http\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;

class TransactionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>'required',
            'account_id'=>'required',
            'account_id'=>'required',
            'paid_at'=>'required',
            'amount'=>'required',
            'currency_code'=>'required',
            'currency_rate'=>'required',
            'description'=>'required',
            'category_id'=>'required',
            'category_id'=>'required',
            'payment_method'=>'required',
            'reference'=>'required',
            'reconciled'=>'required',
        ];
    }
}
