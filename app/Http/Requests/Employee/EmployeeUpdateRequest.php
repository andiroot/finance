<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branch_id'=>'required',
            'user_id'=>'required',
            'department_id'=>'required',
            'designation_id'=>'required',
            'account_id'=>'required',
            'nip'=>'required',
            'address'=>'required',
            'phone'=>'required',
        ];
    }
}
