<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CustomerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'province_id'=>'required',
            'city_id'=>'required',
            'county_id'=>'required',
            'enabled'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'Name is required',
            'email.required'=>'Email is required',
            'address.required'=>'Address is required',
            'phone.required'=>'Phone is required',
            'province_id.required'=>'Province is required',
            'city_id.required'=>'City is required',
            'county_id.required'=>'County is required',
            'enabled.required'=>'Status is required',
        ];
    }
}
