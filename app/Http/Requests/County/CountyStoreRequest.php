<?php

namespace App\Http\Requests\County;

use Illuminate\Foundation\Http\FormRequest;

class CountyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'province_id'=>'required',
            'province_id'=>'required',
            'city_id'=>'required',
            'city_id'=>'required',
            'name'=>'required',
        ];
    }
}
