<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoice_id'=>'required',
            'order_numebr'=>'required',
            'shipping_id'=>'required',
            'customer_id'=>'required',
            'employee_id'=>'required',
            'product_id'=>'required',
            'employee_id'=>'required',
            'payment_method'=>'required',
            'status'=>'required',
            'net_profit'=>'required',
            'processed_at'=>'required',
            'processed_by'=>'required',
        ];
    }
}
