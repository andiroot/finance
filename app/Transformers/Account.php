<?php

namespace App\Transformers;

use App\Models\Banking\Account as Model;
use League\Fractal\TransformerAbstract;

class Account extends TransformerAbstract
{
    /**
     * @param Model $model
     * @return array
     */
    public function transform(Model $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'bank_number' => $model->number,
            'opening_balance' => $model->opening_balance,
            'bank_name' => $model->bank_name,
            'bank_phone' => $model->bank_phone,
            'bank_address' => $model->bank_address,
            'enabled' => $model->enabled,
            'type'=>$model->type,
            'created_at' => $model->created_at ? $model->created_at->toIso8601String() : '',
            'updated_at' => $model->updated_at ? $model->updated_at->toIso8601String() : '',
        ];
    }
}
