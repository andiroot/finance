<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace'=>'API','prefix'=>'v1'],function (){
    Route::post('login','Users\UserController@login');
    Route::post('register','Users\UserController@register');

    Route::group(['middleware'=> 'auth:api'],function (){
        //using auth:api
        // Route::post('/', 'TaskController@createTask');
        // Route::get('/', 'TaskController@getAllTask');
        // Route::delete('/{id}', 'TaskController@deleteTask');
        // Route::get('/{id}', 'TaskController@getTask');
        // Route::put('/{id}', 'TaskController@updateTask');
        // Route::post('/assign', 'TaskController@assignTaskTo');
        // Route::put('/assign/{id}', 'TaskController@updateAssignTask');

        
        //Banking
        
        Route::group(['prefix'=>'accounts'],function (){
            Route::post('/','Banking\AccountController@createAccount');
            Route::get('/', 'Banking\AccountController@getAllAccount');
            Route::get('/{id}', 'Banking\AccountController@getAccount');
            Route::delete('/{id}','Banking\AccountController@deleteAccount');
            Route::put('/{id}','Banking\AccountController@updateAccount');
        });

        Route::group(['prefix'=>'transactions'],function (){
            Route::post('/','Banking\TransactionController@createTransaction');
            Route::get('/', 'Banking\TransactionController@getAllTransaction');
            Route::get('/{id}', 'Banking\TransactionController@getTransaction');
            Route::delete('/{id}','Banking\TransactionController@deleteTransaction');
            Route::put('/{id}','Banking\TransactionController@updateTransaction');
        });
        
        Route::group(['prefix'=>'transfers'],function (){
            Route::post('/','Banking\TransferController@createTranfer');
            Route::get('/', 'Banking\TransferController@getAll');
            Route::get('/{id}', 'Banking\TransferController@getTranfer');
            Route::delete('/{id}','Banking\TransferController@deleteTranfer');
            Route::put('/{id}','Banking\TransferController@updateTranfer');
        });

        //Common
        Route::group(['prefix'=>'customers'],function (){
            Route::post('/','Income\CustomerController@createCustomer');
            Route::get('/', 'Income\CustomerController@getAllCustomer');
            Route::get('/{id}', 'Income\CustomerController@getCustomer');
            Route::delete('/{id}','Income\CustomerController@deleteCustomer');
            Route::put('/{id}','Income\CustomerController@updateCustomer');
        });

        Route::group(['prefix'=>'categories'],function (){
            Route::post('/','Common\CategoryController@createCategory');
            Route::get('/', 'Common\CategoryController@getAllCategory');
            Route::get('/{id}', 'Common\CategoryController@getCategory');
            Route::delete('/{id}','Common\CategoryController@deleteCategory');
            Route::put('/{id}','Common\CategoryController@updateCategory');
        });
        Route::group(['prefix'=>'provinces'],function (){
            Route::post('/','Common\ProvinceController@createProvince');
            Route::get('/', 'Common\ProvinceController@getAllProvince');
            Route::get('/{id}', 'Common\ProvinceController@getProvince');
            Route::delete('/{id}','Common\ProvinceController@deleteProvince');
            Route::put('/{id}','Common\ProvinceController@updateProvince');
        });
        Route::group(['prefix'=>'cities'],function (){
            Route::post('/','Common\CityController@createCity');
            Route::get('/', 'Common\CityController@getAllCity');
            Route::get('/{id}', 'Common\CityController@getCity');
            Route::delete('/{id}','Common\CityController@deleteCity');
            Route::put('/{id}','Common\CityController@updateCity');
        });
        Route::group(['prefix'=>'counties'],function (){
            Route::post('/','Common\CountyController@createCounty');
            Route::get('/', 'Common\CountyController@getAllCounty');
            Route::get('/{id}', 'Common\CountyController@getCounty');
            Route::delete('/{id}','Common\CountyController@deleteCounty');
            Route::put('/{id}','Common\CountyController@updateCounty');
        });

        Route::group(['prefix'=>'contacts'],function (){
            Route::post('/','Common\ContactController@createContact');
            Route::get('/', 'Common\ContactController@getAllContact');
            Route::get('/{id}', 'Common\ContactController@getContact');
            Route::delete('/{id}','Common\ContactController@deleteContact');
            Route::put('/{id}','Common\ContactController@updateContact');
        });
        Route::group(['prefix'=>'vendors'],function (){
            Route::post('/','Expense\VendorController@createVendor');
            Route::get('/', 'Expense\VendorController@getAllVendor');
            Route::get('/{id}', 'Expense\VendorController@getVendor');
            Route::delete('/{id}','Expense\VendorController@deleteVendor');
            Route::put('/{id}','Expense\VendorController@updateVendor');
        });

        //Income 
       
        Route::group(['prefix'=>'orders'],function (){
            Route::post('/','Income\OrderController@createOrder');
            Route::get('/', 'Income\OrderController@getAllOrder');
            Route::get('/{id}', 'Income\OrderController@getOrder');
            Route::delete('/{id}','Income\OrderController@deleteOrder');
            Route::put('/{id}','Income\OrderController@updateOrder');
        });

        Route::group(['prefix'=>'revenues'],function (){
            Route::post('/','Income\RevenueController@createRevenues');
            Route::get('/', 'Income\RevenueController@getAllRevenues');
            Route::get('/{id}', 'Income\RevenueController@getRevenues');
            Route::delete('/{id}','Income\RevenueController@deleteRevenues');
            Route::put('/{id}','Income\RevenueController@updateRevenues');
        });


         //Expense
         Route::group(['prefix'=>'payments'],function (){
            Route::post('/','Expense\PaymentController@createPayment');
            Route::get('/', 'Expense\PaymentController@getAllPayment');
            Route::get('/{id}', 'Expense\PaymentController@getPayment');
            Route::delete('/{id}','Expense\PaymentController@deletePayment');
            Route::put('/{id}','Expense\PaymentController@updatePayment');
        });
        
        Route::group(['prefix'=>'users'],function (){
            Route::get('profile','Users\UserController@profile');
            Route::get('findUser','Users\UserController@search');
            Route::put ('profile','Users\UserController@updateProfile');
            Route::apiResources(['/'=>'Users\UserController']);
        });
        //Payroll
        Route::group(['prefix'=>'departments'],function (){
            Route::post('/','Payroll\DepartmentController@createDepartment');
            Route::get('/', 'Payroll\DepartmentController@getAllDepartment');
            Route::get('/{id}', 'Payroll\DepartmentController@getDepartment');
            Route::delete('/{id}','Payroll\DepartmentController@deleteDepartment');
            Route::put('/{id}','Payroll\DepartmentController@updateDepartment');
        });

        Route::group(['prefix'=>'designations'],function (){
            Route::post('/','Payroll\DesignationController@createDesignation');
            Route::get('/', 'Payroll\DesignationController@getAllDesignation');
            Route::get('/{id}', 'Payroll\DesignationController@getDesignation');
            Route::delete('/{id}','Payroll\DesignationController@deleteDesignation');
            Route::put('/{id}','Payroll\DesignationController@updateDesignation');
        });
        Route::group(['prefix'=>'employees'],function (){
            Route::post('/','Payroll\EmployeeController@createEmployee');
            Route::get('/', 'Payroll\EmployeeController@getAllEmployee');
            Route::get('/{id}', 'Payroll\EmployeeController@getEmployee');
            Route::delete('/{id}','Payroll\EmployeeController@deleteEmployee');
            Route::put('/{id}','Payroll\EmployeeController@updateEmployee');
        });
        Route::group(['prefix'=>'departments'],function (){
            Route::post('/','Payroll\DepartmentController@createDepartment');
            Route::get('/', 'Payroll\DepartmentController@getAllDepartment');
            Route::get('/{id}', 'Payroll\DepartmentController@getDepartment');
            Route::delete('/{id}','Payroll\DepartmentController@deleteDepartment');
            Route::put('/{id}','Payroll\DepartmentController@updateDepartment');
        });
        //Inventory
        Route::group(['prefix'=>'inventories'],function (){
            Route::post('/','Inventory\InventoryController@createInventory');
            Route::get('/', 'Inventory\InventoryController@getAllInventory');
            Route::get('/{id}', 'Inventory\InventoryController@getInventory');
            Route::delete('/{id}','Inventory\InventoryController@deleteInventory');
            Route::put('/{id}','Inventory\InventoryController@updateInventory');
        });
        Route::group(['prefix'=>'packages'],function (){
            Route::post('/','Inventory\PackageController@createPackage');
            Route::get('/', 'Inventory\PackageController@getAllPackage');
            Route::get('/{id}', 'Inventory\PackageController@getPackage');
            Route::delete('/{id}','Inventory\PackageController@deletePackage');
            Route::put('/{id}','Inventory\PackageController@updatePackage');
        });

        Route::group(['prefix'=>'products'],function (){
            Route::post('/','Inventory\ProductController@createProduct');
            Route::get('/', 'Inventory\ProductController@getAllProduct');
            Route::get('/{id}', 'Inventory\ProductController@getProduct');
            Route::delete('/{id}','Inventory\ProductController@deleteProduct');
            Route::put('/{id}','Inventory\ProductController@updateProduct');
        });

        Route::group(['prefix'=>'uoms'],function (){
            Route::post('/','Inventory\UomController@createUom');
            Route::get('/', 'Inventory\UomController@getAllUom');
            Route::get('/{id}', 'Inventory\UomController@getUom');
            Route::delete('/{id}','Inventory\UomController@deleteUom');
            Route::put('/{id}','Inventory\UomController@updateUom');
        });


       

       

        //using auth:api
    });
});


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


//Banking
// Route::apiResources(['account'=>'API\Banking\AccountController']);

// //common

// Route::apiResources(['customerr'=>'API\Common\CustomerController']);
// Route::apiResources(['province'=>'API\Common\ProvinceController']);
// Route::apiResources(['city'=>'API\Common\CityController']);
// Route::apiResources(['county'=>'API\Common\CountyController']);

// //income
// Route::apiResources(['order'=>'API\Income\OrderController']);
// Route::apiResources(['revenue'=>'API\Income\RevenueController']);

// Route::apiResources(['user'=>'API\UserController']);
// Route::apiResources(['deal'=>'API\DealController']);
// Route::apiResources(['deal'=>'API\ProductController']);


