<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//test usercontrollerRepositories
//Route::get('/user','UserRepositoryController@index');
//Route::get('/user/{params}','API\UserControllerRepositories@show');
Auth::routes();

Route::get('/home', 'API\HomeController@index')->name('home');
Route::get('invoice',function(){
    return view('invoice');
}); 

//Route::get('{path}',"HomeController@index")->where( 'path' , '([A-z\d-\/_.]+)?' );
Route::get('{path}','API\HomeController@index')->where( 'path', '([A-z\d/_.]+)?' );


