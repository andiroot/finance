/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment';
import { Form, HasError, AlertError } from 'vform'
window._ = require('lodash');

//import gate global class
import Gate from "./Gate.js";
//prototype biar bisa di akses di semua frontend/ui Vue
Vue.prototype.$gate = new Gate(window.user);


//sheetalert2
import swal from 'sweetalert2'
window.swal = swal;

//instans
const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})
window.toast = toast;

//Bootstrap vue
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

//vue select
import vSelect from 'vue-select'
Vue.component('v-select',vSelect)
//vue tab


//tabs

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

//register vue pagination
Vue.component('pagination', require('laravel-vue-pagination'));

import VueRouter from 'vue-router'
Vue.use(VueRouter)

//buat progress bar sbelumnya install npm install vue-progressbar
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
})



const routes = [
    //Banking
    { path: '/accounts', component: require('./components/Accounts.vue').default },
    //Common
    { path: '/customers', component: require('./components/Customers.vue').default },
    { path: '/contacts', component: require('./components/Contacts.vue').default },
    { path: '/couriers', component: require('./components/Couriers.vue').default },
    { path: '/designations', component: require('./components/Designation.vue').default },
    { path: '/departments', component: require('./components/Department.vue').default },
    { path: '/branchs', component: require('./components/Branch.vue').default },
    { path: '/employees', component: require('./components/Employee.vue').default },
    
    //Income
    { path: '/orders', component: require('./components/Orders.vue').default },
    { path: '/orderss', component: require('./components/orderss.vue').default },
    //
    //Expense
    { path: '/vendors', component: require('./components/Vendors.vue').default },

    //Setting
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/deals', component: require('./components/Deals.vue').default },
    { path: '/dealss', component: require('./components/Dealss.vue').default },
    { path: '/order', component: require('./components/Order.vue').default },
    { path: '/invoices', component: require('./components/Invoices.vue').default },
    { path: '/users', component: require('./components/Users.vue').default },
    { path: '/developers', component: require('./components/Developer.vue').default },
    { path: '/profile', component: require('./components/Profile.vue').default },
    { path: '*', component: require('./components/NotFound.vue').default },
]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`

})

Vue.filter('upText', function(text) {
    //return text.toUpperCase();
    return text.charAt(0).toUpperCase() + text.slice(1);
});
Vue.filter('myDate', function(created) {
    return moment(created).format('MMMM Do YYYY');
});


window.Fire = new Vue();
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

//notfound
Vue.component(
    'not-found',
    require('./components/NotFound.vue').default
);



Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
    router,

    data:{
        search: ''
    },
    methods:{
        //if searchit call ,wait for 2second and fire the searching
        searchit: _.debounce(()=>{
            //console.log("searching.."); 
            //custom event
            Fire.$emit('searching');

        },1000),
        printme(){
            window.print();
        }
    }   
});