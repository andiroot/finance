<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('inv_number');
            $table->unsignedBigInteger('order_id')->index();
            $table->date('invoiced_at');
            $table->date('due_at');
            $table->double('amount', 15, 4);
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->string('notes');
            $table->string('attachment');
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::create('invoice_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invoice_id')->index();
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');
            $table->string('qty');
            $table->double('total');
            $table->unsignedBigInteger('tax_id');
            $table->foreign('tax_id')->references('id')->on('taxes');
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::create('invoice_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::create('invoice_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invoice_id')->index();
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->unsignedBigInteger('account_id');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->date('paid_at');
            $table->double('amount');
            $table->string('description');
            $table->enum('payment_method',['COD','TRANSFER']);
            $table->string('references');
            $table->string('attachment');
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::create('invoice_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invoice_id')->index();
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->string('status_code');
            $table->string('notify');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::create('invoice_totals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invoice_id')->index();
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->string('code');
            $table->string('name');
            $table->string('amount');
            $table->string('sort_order');
            $table->timestamps();
            $table->softDeletes();

        });
        Schema::create('invoice_product_taxes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invoice_id')->index();
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->unsignedBigInteger('invoice_product_id');
            $table->foreign('invoice_product_id')->references('id')->on('invoice_products');
            $table->integer('tax_id');
            $table->double('amount');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('invoice_products');
        Schema::dropIfExists('invoice_status');
        Schema::dropIfExists('invoice_payments');
        Schema::dropIfExists('invoice_histories');
        Schema::dropIfExists('invoice_totals');
        Schema::dropIfExists('invoice_product_taxes');
    }
}
