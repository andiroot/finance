<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vendor_id')->index();
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedBigInteger('uom_id');
            $table->foreign('uom_id')->references('id')->on('uoms');
            $table->unsignedBigInteger('tax_id');
            $table->foreign('tax_id')->references('id')->on('taxes');
            $table->string('name');
            $table->string('sku');
            $table->string('description');
            $table->double('sale_price', 15, 4);
            $table->double('purchase_price', 15, 4);
            $table->double('capital_price');
            $table->integer('qty');
            $table->integer('weight');
            $table->integer('price_closing');
            $table->boolean('enabled');
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('product_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->foreign('product_id')->references('id')->on('products');
            $table->string('photo');
            $table->boolean('enabled');
            $table->timestamps();
            $table->softDeletes();

            $table->index('product_id');
        });
       
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_photos');
        
    }
}
