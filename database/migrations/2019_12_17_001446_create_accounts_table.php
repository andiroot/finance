<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('bank_number');
            $table->string('bank_name');
            $table->string('bank_phone');
            $table->string('bank_address');
            $table->double('opening_balance', 15, 4)->default('0.0000');
            $table->enum('type',['Piutang','Hutang','Kas','Retur','Diskon','Shipping','Packing','Revenue','Expense','Bank']);
            $table->boolean('enabled');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
