<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bill_number');
            $table->unsignedBigInteger('order_id')->index();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->string('bill_status_code');
            $table->date('billed_at');
            $table->date('due_at');
            $table->double('amount', 15, 4);
            $table->unsignedBigInteger('vendor_id');
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->string('notes');
            $table->string('attachment');
            $table->timestamps();
            $table->softDeletes();
            
        });
        Schema::create('bill_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_id');
            $table->foreign('bill_id')->references('id')->on('bills');
            $table->integer('product_id');
            $table->foreign('product_id')->references('id')->on('products');
            $table->string('qty');
            $table->double('total');
            $table->integer('tax_id');
            $table->foreign('tax_id')->references('id')->on('taxes');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });
        Schema::create('bill_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });
        Schema::create('bill_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_id');
            $table->foreign('bill_id')->references('id')->on('bills');
            $table->integer('account_id');
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->date('paid_at');
            $table->double('amount');
            $table->string('description');
            $table->enum('payment_method',['COD','TRANSFER']);
            $table->string('references');
            $table->string('attachment');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });
        Schema::create('bill_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_id');
            $table->foreign('bill_id')->references('id')->on('bills');
            $table->string('status_code');
            $table->string('notify');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });
        Schema::create('bill_totals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_id');
            $table->foreign('bill_id')->references('id')->on('bills');
            $table->string('code');
            $table->string('name');
            $table->string('amount');
            $table->string('sort_order');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });
        Schema::create('bill_product_taxes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bill_id');
            $table->foreign('bill_id')->references('id')->on('bills');
            $table->integer('bill_product_id');
            $table->foreign('bill_product_id')->references('id')->on('bill_products');
            $table->integer('tax_id');
            $table->double('amount');
            $table->timestamps();
            $table->softDeletes();

            $table->index('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
        Schema::dropIfExists('bill_products');
        Schema::dropIfExists('bill_status');
        Schema::dropIfExists('bill_payments');
        Schema::dropIfExists('bill_histories');
        Schema::dropIfExists('bill_totals');
        Schema::dropIfExists('bill_product_taxes');
    }
}
